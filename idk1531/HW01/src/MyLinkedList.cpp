//
// Created by rossm on 3/18/2018.
//

#include <clocale>
#include <iostream>
#include "MyLinkedList.h"


using namespace std;

MyLinkedList::MyLinkedList() {
    this->head = nullptr;
}

MyLinkedList::MyLinkedList(int n, int v) {
    this->head = nullptr;

    for(int i = 0; i < n; i++) {
        this->push_front(v);
    }
}

MyLinkedList::MyLinkedList(const MyLinkedList &list):head(nullptr){
    Node *cur = list.head;
    while (cur != nullptr)
    {
        push_back(cur->value);
        cur = cur->prtNode;
    }
}

size_t MyLinkedList::getLength() const {

    if(head == nullptr) return 0;

    size_t s = 0;
    Node *head = this->head;

    while(head != nullptr){
        s++;
        head = head->prtNode;
    }
    return s;
}

void MyLinkedList::push_front(int v) {
    Node *newNode = new Node();
    newNode->value = v;
    newNode->prtNode = this->head;
    this->head = newNode;
}

void MyLinkedList::push_back(int v) {
    Node *newNode = new Node();
    newNode->value = v;
    Node *currentNode = nullptr;
    Node *nextNode = this->head;

    if (nextNode == nullptr) {
        this->head = newNode;
        return;
    }

    while(nextNode != nullptr){
        currentNode = nextNode;
        nextNode = nextNode->prtNode;
    }

    newNode->prtNode = nextNode;
    currentNode->prtNode = newNode;

}

std::ostream &operator<<(std::ostream &os, const MyLinkedList &myLinkedList) {
    for ( size_t x = 0; x < myLinkedList.getLength(); x++) {
        os <<  myLinkedList[x] << " ";
    }
    return os;

}

int &MyLinkedList::operator[](size_t x) const {
    Node *head = this->head;

    for (int y = 0; y < int(x); y++) {
        head = head->prtNode;
    }
    return head->value;
}

bool operator==(const MyLinkedList &lhs, const MyLinkedList &rhs) {
    size_t count = 0;
    if (lhs.getLength() == rhs.getLength()) {
        int elements = int(lhs.getLength());
        for (int x = 0; x < elements; x++) {
            if (lhs[x] == rhs[x]) {
                count++;
            }
        }
    }
    if (count == lhs.getLength()) { return true; }
    return false;
}

void MyLinkedList::insert(size_t p, int v) {
    Node *pre = new Node();
    Node *cur = new Node();
    Node *temp = new Node();
    cur = head;

    if (p == 0) {
        temp->value = v;
        temp->prtNode = this->head;
        this->head = temp;
    }

    for (int i = 0; i < int(p); i++) {
        pre = cur;
        cur = cur->prtNode;
    }

    temp->value = v;
    pre->prtNode = temp;
    temp->prtNode = cur;
}

void MyLinkedList::remove(size_t p) {
    Node *pre = new Node();
    Node *cur = new Node();
    Node *next = new Node();
    cur = head;

    if (p == 0) {
        this->head = head->prtNode;
    }

    for (int i = 0; i < int(p); i++) {
        pre = cur;
        cur = cur->prtNode;
        next = cur->prtNode;
    }
    pre->prtNode = next;
}

void MyLinkedList::applyUnaryOperator(const MyLinkedList &myLinkedList, int (*unaryOperator)(int)) {
    for ( int i = 0; i < myLinkedList.getLength(); i++ ) {
        myLinkedList[i] = unaryOperator(myLinkedList[i]);
    }
}

