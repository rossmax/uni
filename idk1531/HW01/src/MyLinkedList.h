//
// Created by rossm on 3/18/2018.
//

#ifndef HW01_MYLINKEDLIST_H
#define HW01_MYLINKEDLIST_H


#include <vector>

class Node
{
public:
    Node *prtNode;
    int value;
};


class MyLinkedList {
public:
    Node *head;

    MyLinkedList(); //default constructor
    MyLinkedList(int n, int v); //constructor with parameters
    MyLinkedList(const MyLinkedList &list); //copy constructor

    size_t getLength() const;
    void push_front(int v);
    void push_back(int v);
    void insert(size_t p, int v);
    void remove(size_t p);

    friend std::ostream& operator<< ( std::ostream &os, const MyLinkedList &myLinkedList );
    int &operator[] (size_t x) const; /*[] operator overloading, DONT WORK: shall be possible to read
    elements  but any modifications shall not
    affect the list elements*/
    friend bool operator== ( const MyLinkedList &lhs, const MyLinkedList &rhs );
    static void applyUnaryOperator (const MyLinkedList &myLinkedList, int (*unaryOperator)(int));
};

#endif //HW01_MYLINKEDLIST_H
