
//
// Created by rossm on 4/20/2018.
//

#include "Board.h"
#include <iostream>


Board::Board() {
    start = last = nullptr;
}

Board Board::append(std::shared_ptr<Tile> t) {

    if (start == nullptr) {
        last = start = std::move(t);
    } else {
        last->next = std::move(t);
        last->next->prev = last;
        last = last->next;
    }
    return *this;
}

std::shared_ptr<Tile> Board::getStartTile() {
    if (start == nullptr) {
        throw std::runtime_error("START TILE IS NOT SET!!");
    }
    return start;
}
