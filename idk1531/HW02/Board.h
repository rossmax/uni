//
// Created by rossm on 4/20/2018.
//

#ifndef HW02_BOARD_H
#define HW02_BOARD_H


#include "Tile.h"

class Board {
public:
    Board();

    Board append(std::shared_ptr<Tile> t);

    std::shared_ptr<Tile> getStartTile();

private:
    std::shared_ptr<Tile> start, last;
};


#endif //HW02_BOARD_H
