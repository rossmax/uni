//
// Created by rossm on 4/23/2018.
//

#include "Card.h"

std::string PrizeCard::getCardName() const {
    std::string res = "prize";
    return res;
}

std::string VitalityCard::getCardName() const {
    std::string res = "vitality";
    return res;
}

std::string StickyCard::getCardName() const {
    std::string res = "sticky";
    return res;
}

std::string DispelCard::getCardName() const {
    std::string res = "dispel";
    return res;
}

std::string RestlessExplorerCard::getCardName() const {
    std::string res = "restlessexplorer";
    return res;
}

std::string DistractCard::getCardName() const {
    std::string res = "distract";
    return res;
}

std::string RewindCard::getCardName() const {
    std::string res = "rewind";
    return res;
}

std::string ZippyCard::getCardName() const {
    std::string res = "zippy";
    return res;
}

std::string HypnoticCard::getCardName() const {
    std::string res = "hypnotic";
    return res;
}
