//
// Created by rossm on 4/23/2018.
//

#ifndef HW02_CARD_H
#define HW02_CARD_H


#include <string>

struct Card {
public:
    virtual std::string getCardName() const = 0;
};

struct ModifierCard {
};
struct ActionCard {
};

struct PrizeCard : public Card, public ModifierCard {
public:
    std::string getCardName() const;
};

struct VitalityCard : public Card, public ModifierCard {
public:
    std::string getCardName() const;
};

struct StickyCard : public Card, public ModifierCard {
public:
    std::string getCardName() const;
};

struct DispelCard : public Card, public ModifierCard {
public:
    std::string getCardName() const;
};

struct RestlessExplorerCard : public Card, public ModifierCard {
public:
    std::string getCardName() const;
};

struct DistractCard : public Card, public ActionCard {
public:
    std::string getCardName() const;
};

struct RewindCard : public Card, public ActionCard {
public:
    std::string getCardName() const;
};

struct ZippyCard : public Card, public ActionCard {
public:
    std::string getCardName() const;
};

struct HypnoticCard : public Card, public ActionCard {
public:
    std::string getCardName() const;
};


#endif //HW02_CARD_H
