//
// Created by rossm on 4/20/2018.
//

#include "Player.h"
#include <algorithm>
#include <ostream>
#include <sstream>
#include <iostream>
#include <algorithm>

Player::Player(const std::string &playerName, std::shared_ptr<Tile> &&startTile) {
    name = playerName;
    current = start = std::move(startTile);
    vitality = 3;
    maxVitality = 3;
    points = 0;
    current->startGameEnter(this);
}

std::ostream &operator<<(std::ostream &os, const Player &player) {
    return os << player.name;
}

void Player::forward() {
    if (current->next == nullptr) {
        std::stringstream ss;
        ss << *current << " IS THE LAST TILE IN THE SEQUENCE, CANNOT MOVE FORWARD!!";
        throw std::runtime_error(ss.str());
    }
    current->leave(this);
    current = current->next;
    current->enter(this);
}

void Player::backward() {
    if (current->prev == nullptr) {
        std::stringstream ss;
        ss << *current << " IS THE FIRST TILE IN THE SEQUENCE, CANNOT MOVE BACKWARD!!";
        throw std::runtime_error(ss.str());
    }
    current->leave(this);
    current = current->prev;
    current->enter(this);
}

void Player::restart() {
    current->leave(this);
    current = start;
    current->enter(this);
}

void Player::sleep() {
    std::cout << this->name << " slept and vitality is restored" << std::endl;
    vitality = maxVitality;
}

size_t Player::getVitality() {
    return vitality;
}

size_t Player::getMaxVitality() {
    return maxVitality;
}

void Player::pickUp() {
    if (current->cards.size() == 0) {
        std::cout << name << " TILE DOES NOT HAVE CARDS!!" << std::endl;
    } else if (cards.size() >= 5 && this->getPlayerType() == "normalplayer" ||
               cards.size() >= 3 && this->getPlayerType() == "flyingplayer") {
        std::cout << name << " CANNOT PICKUP CARDS, BECAUSE ARLEADY HAVE " << cards.size() << " CARDS!!" << std::endl;
    } else {
        if (current->getFirstCard()->getCardName() == "vitality") {
            maxVitality += 1;
            vitality += 1;
            cards.push_back(current->getFirstCard());
            std::cout << name << " pick up " << current->getFirstCard()->getCardName() << " card" << std::endl;
            current->resetFirst();
            return;
        }
        if (current->getFirstCard()->getCardName() == "prize") {
            points += 1;
            cards.push_back(current->getFirstCard());
            std::cout << name << " pick up " << current->getFirstCard()->getCardName() << " card" << std::endl;
            current->resetFirst();
            return;
        }

        if (current->getFirstCard()->getCardName() == "distract" && cards.size() != 0) {
            int ran = (rand() % (cards.size() - 1)) + 1;
            std::cout << name << " pick up " << current->getFirstCard()->getCardName() << " card" << std::endl;
            drop(cards[ran]);
            cards.push_back(current->getFirstCard());
            current->resetFirst();
            /*drop(cards[cards.size() - 1]);*/
            return;
        }

        if (current->getFirstCard()->getCardName() == "rewind") {
            cards.push_back(current->getFirstCard());
            std::cout << name << " pick up " << current->getFirstCard()->getCardName() << " card" << std::endl;
            current->resetFirst();
            /*drop(cards[cards.size() - 1]);*/
            cards.clear();
            current = start;
            current->startGameEnter(this);
            return;
        }

        if (current->getFirstCard()->getCardName() == "zippy") {
            cards.push_back(current->getFirstCard());
            std::cout << name << " pick up " << current->getFirstCard()->getCardName() << " card" << std::endl;
            current->resetFirst();
            /*drop(cards[cards.size() - 1]);*/
            this->vitality += 1;
            return;
        }

        if (current->getFirstCard()->getCardName() == "hypnotic") {
            cards.push_back(current->getFirstCard());
            std::cout << name << " pick up " << current->getFirstCard()->getCardName() << " card" << std::endl;
            current->resetFirst();
            /*drop(cards[cards.size() - 1]);*/
            this->vitality = 0;
            return;
        }

        if (current->getFirstCard()->getCardName() == "dispel" ||
            current->getFirstCard()->getCardName() == "sticky" ||
            current->getFirstCard()->getCardName() == "restlessexplorer") {
            cards.push_back(current->getFirstCard());
            std::cout << name << " pick up " << current->getFirstCard()->getCardName() << " card" << std::endl;
            current->resetFirst();
            return;
        }
    }

}

std::vector<std::shared_ptr<const Card>> Player::getCards() {
    return cards;
}

std::vector<std::shared_ptr<const Card>> Player::getCardsByName(std::string name) {
    std::vector<std::shared_ptr<const Card>> result;
    for (int i = 0; i < cards.size(); i++) {
        if (cards[i]->getCardName() == name) {
            result.push_back(cards[i]);
        }
    }
    return result;
}

std::shared_ptr<const Card> &Player::getCardByName(std::string name) {
    for (int i = 0; i < cards.size(); i++) {
        if (cards[i]->getCardName() == name) {
            return cards[i];
        }
    }
}

void Player::drop(std::shared_ptr<const Card> &card) {
    if (std::find(cards.begin(), cards.end(), card) == cards.end()) {
        std::cout << name << " CANNOT DROP CARD THAT DOES NOT EXIST!!" << std::endl;
        return;
    }

    if (card->getCardName() == "sticky") {
        std::cout << name << " CANNOT DROP STICKY CARD!!" << std::endl;
        return;
    }

    if (card->getCardName() == "vitality") {
        maxVitality -= 1;
    }

    for (int i = 0; i < cards.size(); i++) {
        if (cards[i] == card) {
            std::cout << name << " dropt " << card->getCardName() << " card" << std::endl;
            current->addCard(cards[i]);
            cards.erase(cards.begin() + i);
            return;
        }
    }
}

std::string Player::getPlayerType() const {
    std::string res = "normalplayer";
    return res;
}

std::string FlyingPlayer::getPlayerType() const {
    std::string res = "flyingplayer";
    return res;
}

