//
// Created by rossm on 4/20/2018.
//

#ifndef HW02_PLAYER_H
#define HW02_PLAYER_H


#include <string>
#include <list>
#include "Tile.h"

class Player {
    friend class Tile;

    friend class ExitWonderlandTile;

public:
    explicit Player(const std::string &playerName, std::shared_ptr<Tile> &&startTile);

    friend std::ostream &operator<<(std::ostream &os, const Player &player);

    void forward();

    void backward();

    void restart();

    void sleep();

    size_t getVitality();

    size_t getMaxVitality();

    void pickUp();

    std::vector<std::shared_ptr<const Card>> getCards();

    std::vector<std::shared_ptr<const Card>> getCardsByName(std::string name);// return a rabnge of what type????
    std::shared_ptr<const Card> &getCardByName(std::string name);

    void drop(std::shared_ptr<const Card> &card);

    virtual std::string getPlayerType() const;

private:
    std::string name;
    std::shared_ptr<Tile> start, current;
    size_t vitality;
    size_t maxVitality;
    std::vector<std::shared_ptr<const Card>> cards;
    size_t points;
};

class FlyingPlayer : public Player {
public:
    explicit FlyingPlayer(const std::string &playerName, std::shared_ptr<Tile> &&startTile) : Player(playerName,
                                                                                                     std::move(
                                                                                                             startTile)) {}

    std::string getPlayerType() const;
};

#endif //HW02_PLAYER_H
