//
// Created by rossm on 4/18/2018.
//

#include <iostream>
#include <sstream>
#include "Tile.h"
#include "Player.h"

Tile::Tile() {
    prev = next = nullptr;
}

bool Tile::canEnter(const Player &playerName) {
    for ( int i = 0; i < playerName.cards.size(); i++ ) {
        if ( playerName.cards[i]->getCardName() == "dispel" ) {
            return true;
        }
    }
    return false;
}

Tile::Tile(const std::string &tileName) {
    name = tileName;
    prev = next = nullptr;
}

std::ostream &operator<<(std::ostream &os, const Tile &tile) {
    return os << "TILE " << tile.name;
}

void Tile::enter(Player *player) {
    if ( player->vitality == 0 ) {
        std::cout << *player << " TOO TIRED TO MOVE!!" << std::endl;
        return;
    }

    for ( int i = 0; i < player->cards.size(); i++ ) {
        if ( player->cards[i]->getCardName() == "restlessexplorer" ) {
            std::cout << *player << " entered tile " << name
                      << " and has unlimited vitality" << std::endl;
            return;
        }
    }

    player->vitality -= 1;
    std::cout << *player << " entered tile " << name
              << " and has " << player->vitality << " vitality" << std::endl;
}

void Tile::startGameEnter(Player *player) {
    std::cout << *player << " entered tile " << name
              << " and has " << player->vitality << " vitality" << std::endl;
}

void Tile::leave(Player *player) {
    if ( player->vitality == 0 ) {
        return;
    }
    std::cout << *player << " left tile " << name << std::endl;
}


void ExitWonderlandTile::enter(Player *player) {
    if ( player->vitality == 0 ) {
        std::cout  << *player << " TOO TIRED TO MOVE!!" << std::endl;
        return;
    }

    if (canEnter(*player)) {
        player->vitality -= 1;
        std::cout << *player <<" sUcCeFuLy eNtEreD ExItWoNdRrLaNdTiLe AnD hAvE "  << player->points <<" PoInTs!" << std::endl;
    } else {
        std::cout  << *player << " DONT HAVE DISPEL CARD TO ENTER EXITWONDERLAND TILE!!" << std::endl;
    }

}

void ExitWonderlandTile::leave(Player *player) {
    if ( player->vitality == 0 ) {
        return;
    }
    std::cout << *player << " left tile " << name << std::endl;
}

void Tile::addCard(std::shared_ptr<const Card> &c) {
    cards.push(c);
}

std::shared_ptr<const Card> Tile::getFirstCard() {
    return cards.front();
}

std::queue<std::shared_ptr<const Card>> Tile::getCards() {
    if ( cards.empty() ) {
        std::cout << " TILE DONT HAVE ANY CARDS" << std::endl;
    }
    return cards;
}

void Tile::reset() {
    while(!cards.empty()) cards.pop();
}

void Tile::resetFirst() {
    this->cards.pop();
}

