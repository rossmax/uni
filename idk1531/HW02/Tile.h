//
// Created by rossm on 4/18/2018.
//

#ifndef HW02_TILE_H
#define HW02_TILE_H


#include <string>
#include <memory>
#include <ostream>
#include <queue>
#include "Card.h"

class Player;

class Tile {
    friend class Board;

    friend class Player;

public:
    Tile();

    explicit Tile(const std::string &tileName);

    friend std::ostream &operator<<(std::ostream &os, const Tile &tile);

    bool canEnter(const Player &playerName);

    virtual void enter(Player *player);

    virtual void leave(Player *player);

    void startGameEnter(Player *player);

    void addCard(std::shared_ptr<const Card> &c);

    std::shared_ptr<const Card> getFirstCard();

    std::queue<std::shared_ptr<const Card>> getCards();

    void reset();

    void resetFirst();

protected:
    std::string name;
    std::shared_ptr<Tile> prev, next;
    std::queue<std::shared_ptr<const Card>> cards;
};


class ExitWonderlandTile : public Tile {
public:
    explicit ExitWonderlandTile() : Tile() {}

    void enter(Player *player) override;

    void leave(Player *player) override;
};

#endif //HW02_TILE_H
