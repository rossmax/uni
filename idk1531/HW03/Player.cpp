//
// Created by rossm on 4/25/2018.
//

#include <iostream>
#include <chrono>
#include <thread>
#include "Player.h"
#include "Room.h"


Player::Player(const std::string &playerName, std::shared_ptr<Room> ptr) {
    name = playerName;
    room = ptr;
    vitality = 1;
    alive = true;
    wasInFightArleady = false;
}

//thread entry point/
void Player::operator()() {

    for (int i = 0; i < 50; i++) {
        room->enter(*this);
        std::this_thread::sleep_for(std::chrono::seconds(2));
        room->action(*this);
        std::this_thread::sleep_for(std::chrono::seconds(2));
        room->leave(*this);
    }
    std::cout << "END OF THE GAME" << std::endl;

}

bool operator==(const Player &lhs, const Player &rhs) {
    return lhs.name == rhs.name;
}

std::string Player::getPlayerType() {
    if (name.find("Warrior",0) == 0) {
        return "Warrior";
    }
    if (name.find("Wizzard",0) == 0) {
        return "Wizzard";
    }
    if (name.find("Monster",0) == 0) {
        return "Monster";
    }
    return "None";
}


