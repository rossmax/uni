//
// Created by rossm on 4/25/2018.
//

#ifndef PRAX13_PLAYER_H
#define PRAX13_PLAYER_H

#include <string>
#include <memory>

class Room; // forward decalring calss Room

class Player {
    friend class Room;
public:
    Player(const std::string &playerName, std::shared_ptr<Room> ptr);
    void operator()();
    friend bool operator== ( const Player &lhs, const Player &rhs );
    std::string getPlayerType();
private:
    std::string name;
    std::shared_ptr<Room> room;
    int vitality;
    bool alive;
    bool wasInFightArleady;
};


#endif //PRAX13_PLAYER_H
