//
// Created by rossm on 4/25/2018.
//

#include <iostream>
#include <algorithm>
#include <random>
#include "Room.h"

extern std::mutex msgmutex;


Room::Room(const std::string &roomName, size_t max) {
    name = roomName;
    maxPlayers = max;
}

void Room::setNext(std::shared_ptr<Room> nextRoom1, std::shared_ptr<Room> nextRoom2) {
    nextRoomsVariables.push_back(nextRoom1);//horizontally
    nextRoomsVariables.push_back(nextRoom2);//vertically
}

std::shared_ptr<Room> Room::getNext() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::bernoulli_distribution d(0.5);
    int index = d(gen) ? 0 : 1;
    return nextRoomsVariables[index];
}

void Room::enter(Player &player) {
    std::unique_lock<std::mutex> lk(m);//try to lock mutex
    cv.wait(lk,[this, &player]{
        auto sz = players.size();
        std::lock_guard<std::mutex> ml (msgmutex);
        player.wasInFightArleady = false;
        if (sz < maxPlayers ) {
            std::cout << player.name
                      <<" entered the room "
                      << name << " with "
                      << sz << " players."
                      << std::endl;
            return true;
        } else {
                std::cout << player.name <<" is waiting to enter the room "
                          << name << " whitch has "
                          << sz << " players."
                          << std::endl;
                return false;
            }
    });

    //here we own the lock and we can do all we want
    players.push_back(player);

}

void Room::leave(Player &player) {
    {
        std::lock_guard<std::mutex> lk(m);
        auto it = std::find(players.begin(), players.end(), player);
        if (it == players.end()) {
            // if player is not find, player not in this room
            return;
        }
        //delete player
        players.erase(it);
        std::lock_guard<std::mutex> ml (msgmutex);
        std::cout << player.name <<" has left room " << name << std::endl;

        if (player.vitality <= 0) {
            player.room = std::make_shared<Room>("B2White", 2);
            std::cout << player.name <<" trying RESSURECT " << std::endl;
        }
        if (player.vitality > 0){
            player.room = player.room->getNext();
        }
    }

    //we done, notify everyone
    cv.notify_all();
}

std::string Room::getRoomName() {
    return name;
}

std::string Room::getRoomType() {
    //string::find return position on first charter if string is find
    if (name.find("White",0) == 2) {
        return "White";
    } else {
        return "Red";
    }
}

void Room::action(Player &player) {
    std::unique_lock<std::mutex> lk(m);//try to lock mutex
    cv2.wait(lk,[this, &player]{
        std::lock_guard<std::mutex> ml (msgmutex);
        if (this->getRoomType() == "White") {
            if (player.getPlayerType() == "Warrior") {
                std::cout << player.name
                          << " is improving his strength and weapon handling skills in room "
                          << this->name
                          << std::endl;
                return true;
            }
            if (player.getPlayerType() == "Wizzard") {
                std::cout << player.name
                          << " trains to use magical spells in room "
                          << this->name
                          << std::endl;
                return true;
             }
            if (player.getPlayerType() == "Monster") {
                std::cout << player.name
                          << " sleeps, snoring occasionally in room "
                          << this->name
                          << std::endl;
                return true;
            }
        }
        if (this->getRoomType() == "Red") {
            if (players.size() == 1) {
                std::cout << player.name
                          << " howls from boredom in room "
                          << this->name
                          << std::endl;
                return true;
            }
            if (players.size() > 1 && player.getPlayerType() == "Monster" && this->hasAnothers()
                     && !player.wasInFightArleady && this->hasNoLosers()) {
                fighting(player);
                return true;
            }
            if (players.size() > 1  &&  this->hasMonster() && player.getPlayerType() != "Monster"
                     && !player.wasInFightArleady && this->hasNoLosers()) {
                fighting(player);
                return true;
            }
        }
        return true;
    });

}

void Room::fighting(Player &player) {

        int indAnother1 = -1;
        int indAnother2 = -1;
        int index;

        if(player.getPlayerType() == "Monster") {
            for (auto i = 0; i < players.size(); i++) {
                if (players[i].getPlayerType() != "Monster") {indAnother1 = i;}
                if (players[i].getPlayerType() != "Monster" && indAnother1 != -1) {indAnother2 = i;}
            }

            std::random_device rd;
            std::mt19937 gen(rd());
            std::bernoulli_distribution d(0.5);
            if (indAnother2 == -1) {
                index = indAnother1;
            } else {
                index = d(gen) ? indAnother1 : indAnother2;
            }

            if (player.vitality >= players[index].vitality
                && !player.wasInFightArleady && !players[index].wasInFightArleady) {
                player.vitality += 1;
                players[index].vitality -= 1;
                std::string log = player.name + " with vitality " + std::to_string(player.vitality) + " is stronger than "
                                  + players[index].name + " with vitality " + std::to_string(players[index].vitality);
                fightLogs.push_back(log);
                std::cout << player.name
                          << " started fight with "
                          << players[index].name
                          << " in room "
                          << name
                          << ". Result: "
                          << log
                          << std::endl;
                player.wasInFightArleady = true;
                players[index].wasInFightArleady = true;
                if (players[index].vitality <= 0 ) {players[index].alive = false;}
                std::cout << players[index].name << " is DEAD" << std::endl;
                return;
            }
            else if (player.vitality < players[index].vitality
                     && !player.wasInFightArleady && !players[index].wasInFightArleady) {
                player.vitality -= 1;
                players[index].vitality += 1;
                std::string log = players[index].name + " with vitality " + std::to_string(players[index].vitality) +
                        " is stronger than " + player.name + " with vitality " + std::to_string(player.vitality) ;

                fightLogs.push_back(log);
                std::cout << player.name
                          << " started fight with "
                          << players[index].name
                          << " in room "
                          << name
                          << ". Result: "
                          << log
                          << std::endl;
                player.wasInFightArleady = true;
                players[index].wasInFightArleady = true;
                if (player.vitality <= 0) {player.alive = false;}
                std::cout << player.name << " is DEAD" << std::endl;
                return;
            }

        }

        else {
        for (auto i = 0; i < players.size(); i++) {
            if (players[i].getPlayerType() == "Monster") {indAnother1 = i;}
            if (players[i].getPlayerType() == "Monster" && indAnother1 != -1) {indAnother2 = i;}
        }

        std::random_device rd;
        std::mt19937 gen(rd());
        std::bernoulli_distribution d(0.5);
        if (indAnother2 == -1) {
            index = indAnother1;
        } else {
            index = d(gen) ? indAnother1 : indAnother2;
        }

        if (player.vitality > players[index].vitality
            && !player.wasInFightArleady && !players[index].wasInFightArleady) {
            player.vitality += 1;
            players[index].vitality -= 1;
            std::string log = player.name + " with vitality " + std::to_string(player.vitality) + " is stronger than "
                              + players[index].name + " with vitality " + std::to_string(players[index].vitality);
            fightLogs.push_back(log);
            std::cout << player.name
                      << " started fight with "
                      << players[index].name
                      << " in room "
                      << name
                      << ". Result: "
                      << log
                      << std::endl;
            player.wasInFightArleady = true;
            players[index].wasInFightArleady = true;
            if (players[index].vitality <= 0 ) {players[index].alive = false;}
            std::cout << players[index].name << " is DEAD" << std::endl;
            return;
        }
        else if (player.vitality <= players[index].vitality
                 && !player.wasInFightArleady && !players[index].wasInFightArleady) {
            player.vitality -= 1;
            players[index].vitality += 1;
            std::string log = players[index].name + " with vitality " + std::to_string(players[index].vitality) +
                    " is stronger than " + player.name + " with vitality " + std::to_string(player.vitality);
            fightLogs.push_back(log);
            std::cout << player.name
                      << " started fight with "
                      << players[index].name
                      << " in room "
                      << name
                      << ". Result: "
                      << log
                      << std::endl;
            player.wasInFightArleady = true;
            players[index].wasInFightArleady = true;
            if (player.vitality <= 0) {player.alive = false;}
            std::cout << player.name << " is DEAD" << std::endl;
            return;
        }

    }

}

bool Room::hasMonster() {
    for (auto i = 0; i < players.size(); i++) {
        if (players[i].getPlayerType() == "Monster") {
            return true;
        }
    }
    return false;
}

bool Room::hasAnothers() {
    for (auto i = 0; i < players.size(); i++) {
        if (players[i].getPlayerType() == "Wizzard" || players[i].getPlayerType() == "Warrior") {
            return true;
        }
    }
    return false;
}

bool Room::hasNoLosers() {
    for (auto i = 0; i < players.size(); i++) {
        if (!players[i].wasInFightArleady) {
            return true;
        }
    }
    return false;
}
