//
// Created by rossm on 4/25/2018.
//

#ifndef PRAX13_ROOM_H
#define PRAX13_ROOM_H


#include <string>
#include <vector>
#include <condition_variable>
#include "Player.h"

class Room {
public:
    Room(const std::string &roomName, size_t max);
    void setNext(std::shared_ptr<Room> nextRoom1, std::shared_ptr<Room> nextRoom2);
    std::shared_ptr<Room> getNext();
    void enter(Player &player);
    void leave(Player &player);
    void action(Player &player);
    void fighting(Player &player);
    bool hasMonster();
    bool hasAnothers();
    bool hasNoLosers();
    std::string getRoomName();
    std::string getRoomType();
private:
    std::string name;
    size_t maxPlayers;
    std::vector<Player> players;
    std::condition_variable cv;// notificatioon to th ethreads
    std::condition_variable cv2;// notificatioon to th ethreads
    std::mutex m;
    std::vector<std::shared_ptr<Room>> nextRoomsVariables;
    std::vector<std::string> fightLogs;
};


#endif //PRAX13_ROOM_H
