#include <iostream>
#include <memory>
#include <thread>
#include "Room.h"

std::mutex msgmutex;

int main() {
    auto b2w = std::make_shared<Room>("B2White", 2);
    auto a2r = std::make_shared<Room>("A2Red", 3);
    auto a1w = std::make_shared<Room>("A1White", 2);
    auto b1r = std::make_shared<Room>("B1Red", 3);

    b2w->setNext(a2r,b1r);
    a2r->setNext(b2w,a1w);
    a1w->setNext(b1r,a2r);
    b1r->setNext(a1w,b2w);


    std::vector<std::thread> players;
    for ( auto &name : { "Warrior1", "Wizzard1", "Monster1", "Warrior2", "Wizzard2", "Monster2", "Warrior3", "Wizzard3", "Monster3" } ) {
        players.emplace_back(Player(name,b2w));

    }

    for ( auto &t : players) {
        if (t.joinable()) {
            t.join();
        }
    }

    std::cout << ""<< std::endl;
    return 0;
}