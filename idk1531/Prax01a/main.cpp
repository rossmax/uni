#include <iostream>

class constexp;

using namespace std;

int main() {

    unsigned int a;
    int b,c;

    c = 10; //asigment
    c = 0x2ALL;//hex base
    c = 052; //binary base

    int k = 10u; //u mean unsigned

    int d = 10;//declaration
    int e(10);
    int f(10);

    //c++ 98 syntax typedef unsigned char byte;
//c++ 11 syntax
    using byte = unsigned char;
//define some type aliases
    using salary = int;
    using budget = int;
    salary managerSalary =1000;
    budget projectBudget = 125000;
// decltype operator returns the type of its operand
    int p = 5;
    decltype(p) q = 10; // q is of whathever type p is
//decltype (f()) x; // x has type whatever f() returns
//c98
    const int x_old = 5;
//c11
    constexpr int x = 5;
    constexpr int y = x + 5;
    bool u = 42; //true
    bool v = 0; //false
    bool 2 = -1; //true
    float pi = 3.14159;
    int i = pi; //3
    float pi2 = i; //3.0
    double d= 9/6; // 1.0
    double d2 = 9.0 / 6;
//type casting
//static_cast<T>(x) -> T x
    long double pi (3.14159);
    double d = static_cast<double>(pi);

//с++ auto
//auto x; ERROR unitialized variable
    auto x2 = 5; //Type of x is int
    auto s = string("word");
    auto s1 = "word"s; // starting from c++14;
    constexp auto z = 9/6;
    constexp auto z2 = static_cast<double>(9/6);

    std::cout << "Hello, World!" << std::endl;
    return 0;
}