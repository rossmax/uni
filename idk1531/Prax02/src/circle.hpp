#ifndef PRAX02_CIRCLE_HPP
#define PRAX02_CIRCLE_HPP

/**
 * Create class Circle.
 * A circle is represented by a center point of type Point and radius of type float, let them be private.
 * Create a constructor taking a center Point and radius as arguments
 * Implement public methods:
 *     getCenter() -- method returns the center Point
 *     getRadius() -- method returns radius
 *     setRadius() -- methods takes a new radius as argument, returns nothing
 *     area() -- returns area of the circle as a single-precision floating-point value
 *     circumference() -- returns circumference of the circle as a single-precision floating-point value
 *     hasPoint(Point) -- returns True if the point lies on a circle, false otherwise.
 */

#endif //PRAX02_CIRCLE_HPP
