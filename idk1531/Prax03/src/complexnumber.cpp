#include <iostream>
#include "complexnumber.h"
#include "boost/ptree.hpp"
#include "boost/xml_parser.hpp"

namespace pt = boost::propery_tree;

ComplexNumber::ComplexNumber (int r, int i) {
    this->real = r;
    this->imag = i;
    std::cout << "Constructor" << *this << std::endl;
    return 0;
}

std::iostream& operator<<(std::iostream& os, ComplexNumber& obj) {
    os << "(" << obj.real << "," << obj.imag << "i)";
    return os;
}

ComplexNumber::ComplexNumber(const ComplexNumber& other){
    std::cout << "copy assigment from" << other << std::endl;
    this->real = other.real;
    this->imag = other.imag;
    return *this;
}

ComplexNumber::ComplexNumber(ComplexNumber &&other) {
    std::cout << "Moving from" << o
              << std::endl;
    this->real = other.real; other.real = 0;
    this->imag = other.imag; other.imag = 0;

}

ComplexNumber &ComplexNumber::operator=(ComplexNumber &&other) {
    std::cout << "Moving assigment-from" << other << std::endl;
    this->real = other.real; other.real = 0;
    this->imag = other.imag; other.imag = 0;
    return *this;
}

std::iostream& operator>>(std::istream &is, ComplexNumber &obj){
    is >> obj.real >> obj.imag;
    return is;
}
//infix increment ++a
ComplexNumber &ComplexNumber::operator++() {
    this-> real += 1;
    return *this;
}
//postfix increment a++
ComplexNumber ComplexNumber::operator++(int a) {
    ComplexNumber tmp(*this);
    operator++();
    return ComplexNumber();
}

bool ComplexNumber::operator==(const ComplexNumber& other) {
    return this->real == other.real && this->imag == other.imag;
}

bool operator<(const ComplexNumber &lhs, const ComplexNumber &rhs) {
    return lhs.real < rhs.real || (lhs.real == rhs.real && lhs.imag < rhs.imag);
}


ComplexNumber operator+(ComplexNumber &lhs, const ComplexNumber &rhs) {
    lhs.real += rhs.real;
    lhs.imag += rhs.imag;
    return lhs;
}

ComplexNumber operator-(ComplexNumber &lhs, const ComplexNumber &rhs) {
    lhs.real -= rhs.real;
    lhs.imag -= rhs.imag;
    return lhs;
}

ComplexNumber operator*(ComplexNumber &lhs, const ComplexNumber &rhs) {
    int real = lhs.real * rhs.real - lhs.imag * rhs.imag;
    int imag = lhs.real * rhs.real + lhs.imag * rhs.imag;
    lhs.real = real;
    lhs.imag = imag;
    return lhs;
}

int &ComplexNumber::operator[](std::size_t idx) {
    if(idx == 0) return this->real;
    else return this->imag;
}

void ComplexNumber::load(const std::string &filename) {
    pt::ptree tree;
    pt::read_xml(filename,tree);
    this->real = tree.get("complexNumber.realPart");
    this->imag = tree.get("complexNumber.imagPart");

}

void ComplexNumber::save(const std::string &filename) {
    pt::ptree tree;
    tree.put("complexNumber.realPart",this->real);
    tree.put("complexNumber.imagPart",this->imag);
    pt::write_xml(filename,tree);
}
