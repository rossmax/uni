#ifndef PRAX03_COMPLEXNUMBER_H
#define PRAX03_COMPLEXNUMBER_H


class ComplexNumber {
public:
    ComplexNumber() : real(0), imag(0) {}                  //default constructor
    ComplexNumber(int r, int i);                           // custom constrictor
    ComplexNumber(const ComplexNumber& other);                // copy constrictor
    ComplexNumber(ComplexNumber&& other) noexcept;                     // move constructor
    ComplexNumber& operator=(const ComplexNumber& other);
    ComplexNumber& operator=(ComplexNumber&& other);             //move asigment opeartor
    friend std::iostream& operator<<(std::iostream& os, const ComplexNumber& obj);
    friend std::iostream& operator>>(std::istream& is, ComplexNumber& obj);
    ComplexNumber& operator++();
    ComplexNumber operator++(int a);
    bool operator==(const ComplexNumber& other);
    friend bool operator<(const ComplexNumber& lhs, const ComplexNumber& rhs);
    friend ComplexNumber operator+(ComplexNumber& lhs, const ComplexNumber& rhs);
    friend ComplexNumber operator-(ComplexNumber& lhs, const ComplexNumber& rhs);
    friend ComplexNumber operator*(ComplexNumber& lhs, const ComplexNumber& rhs);
    int& operator[](std::size_t idx);
    void load(const std::string& filename);
    void save(const std::string& filename);

private:
    int real, imag;
};


#endif //PRAX03_COMPLEXNUMBER_H
