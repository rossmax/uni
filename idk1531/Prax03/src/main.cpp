#include <iostream>
#include "complexnumber.h"

int main() {
    ComplexNumber c1(1,1);
    ComplexNumber c2(c1);
    ComplexNumber c3 = c2;
    ComplexNumber c4 = std::move(c1); //moving inizializatipn from xvalue
    ComplexNumber c5(ComplexNumber(5,5));  //move init fore rvalue
    ComplexNumber c6,c7;
    c6 = (ComplexNumber(6,6));
    c7 = std::move(c1);

    //std::cout << "cord [real] [imag]:";
    //std::cin >> c7;
    //std::cout << "c7 =" << c7 << std::endl;
    ComplexNumber c8(2,2), c9(5,10), c10(10,50);
    std::cout << "c8 + c9 = " << c8 ++ c9 << std::endl;
    std::cout << c1 << std::endl;
    return 0;
}