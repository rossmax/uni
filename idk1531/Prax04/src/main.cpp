#include <gtest/gtest.h>
#include "pointers.hpp"

int main(int argc, char **argv) {

    int a = 2, b = 3, c;
    Pointers::sum(&a, &b, &c);
    std::cout << "The sum is " << c << std::endl;

    char buf[1024];
    Pointers::copy("Hello, World!", buf);
    std::cout << buf << std::endl;

    Pointers::reverse("Hello, World!", buf);
    std::cout << buf << std::endl;

    std::cout << Pointers::countLetters("Hello, World!") << std::endl;

    std::cout << Pointers::palindrome("level") << " "
              << Pointers::palindrome("palindrome") << std::endl;

    std::cout << Pointers::compare("Hello,", "World!") << " "
              << Pointers::compare("He","Hello") << " "
              << Pointers::compare("Hello","Hell") << " "
              << Pointers::compare("Hi","Hi") << std::endl;

    const char* s = "Hello, World!";
    const char* p = "{}()[];:.,/-_!#";
    //::testing::InitGoogleTest(&argc, argv);
    //return RUN_ALL_TESTS();

    return 0;

}