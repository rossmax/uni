#include <iostream>
#include <cstring>
#include <thread>

using u_char = unsigned char;    // C++11
typedef unsigned char uchar;     // C++98

using PMsg = void (*)(std::string);   // C++11
typedef void (*PMsg2)(std::string);   // C++98


class A {
public:
    explicit A(int i=0) {
        callbackFunc = nullptr;
    }
    void registerCallback(PMsg func) {
        // save the address of func inside our object
        callbackFunc = func;
    }
    void notify() {
        callbackFunc("I am done with computations.");
    }
private:
    PMsg callbackFunc;
};

void printResult(std::string msg) {
    std::cout << msg << std::endl;
}

float add(float a, float b) { return a + b; }
float mul(float a, float b) { return a * b; }

std::string getShortestWord(const char *s1, const char *s2,
                            int (*comparator)(const char *, const char*)) {
    int result = comparator(s1,s2);
    return result < 1 ? std::string(s1) : std::string(s2);

}

std::string getLongestWord(const char *s1, const char *s2, size_t (*calcLength)(const char*)) {
    return calcLength(s1) < calcLength(s2) ? std::string(s2) : std::string(s1);
}

int main() {

    // uninitialized 1KB buffer on the stack
    u_char x[1024];
    u_char *px = x;
    u_char *px2 = &x[0];   // same as previous

    // 4 bytes reserved on the stack (uninitialized)
    int y;

    // an object of type A is reserved on the stack
    A a(10);

    // allocate 4 bytes (uninitialized) in the heap, pi is a pointer to int
    auto *pi = new int();
    delete pi;

    // allocate 4 bytes with value 1024 in the heap, pi is a pointer to int
    pi = new int(1024);
    delete pi;

    auto *pa = new A(10);

    // uninitialized pointer to function with signature float(float,float)
    float (*pNumOp)(float, float);

    // pNumOp points to float add(float a, float b)
    pNumOp = add;
    std::cout << "pNumOp(1.5,2.5) = " << pNumOp(1.5,2.5) << std::endl;

    // pNumOp points to float mul(float a, float b)
    pNumOp = mul;
    std::cout << "pNumOp(1.5,2.5) = " << pNumOp(1.5,2.5) << std::endl;
    std::cout << "Another way to call the same function " << (*pNumOp)(1.5,2.5) << std::endl;

    const char *s1 = "Hello";
    const char *s2 = "World!";

    std::string shortest = getShortestWord(s1,s2,strcmp);
    std::cout << "The shortest word is: " << shortest << std::endl;
    std::cout << "The longest word is: " << getLongestWord(s1,s2,strlen) << std::endl;

    A *worker = new A();

    worker->registerCallback(printResult);

    std::this_thread::sleep_for(std::chrono::seconds(3));

    worker->notify();

    delete worker;

    return 0;
}