#ifndef PRAX07_BIRD_H
#define PRAX07_BIRD_H

#include <string>
#include <utility>

//using BirdType = enum { t_PIDGEON=0, t_PENGUIN=1 };   // C++11
typedef enum { t_PIDGEON, t_PENGUIN } BirdType;         // C++98

class Bird {
public:
    explicit Bird(const std::string &name, size_t id) : name(name), id(id) {}
    virtual void jump() const;
    virtual void make_noise() const;
    virtual void flap_wings() const;
    virtual void doEverything() const = 0;
protected:
    size_t id;
    std::string name;
};

class FlyingBird : public Bird {
public:
    FlyingBird(const std::string &name, size_t id) : Bird(name,id) {}
    virtual void fly() const;
};

class SwimmingBird : public Bird {
public:
    SwimmingBird(const std::string &name, size_t id) : Bird(name,id) {}
    virtual void swim() const;
};

class Pidgeon final : public FlyingBird {
public:
    Pidgeon() : FlyingBird("pidgeon", ++counter) {}
    void doEverything() const override;
private:
    static size_t counter;
};

class Penguin final : public SwimmingBird {
public:
    Penguin() : SwimmingBird("penguin",++counter) {}
    void doEverything() const override;
private:
    static size_t counter;
};

class BirdFactory {
public:
    static Bird* makeBird(BirdType type);
};

#endif //PRAX07_BIRD_H
