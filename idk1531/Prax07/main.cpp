#include <iostream>
#include <vector>
#include "bird.h"

int main() {

    // cannot instantiate Bird
    // Bird b("bird",0);

    // cannot instantiate FlyingBird
    // FlyingBird("bird",0);

    // cannot instantiate SwimmingBird
    // SwimmingBird("bird",0);

    Pidgeon p,p2;
    Penguin pp;

    Bird *ptr1 = BirdFactory::makeBird(t_PIDGEON);
    Bird *ptr2 = BirdFactory::makeBird(t_PENGUIN);

    ptr1->doEverything();
    ptr2->doEverything();

    Bird &pref = p, &p2ref = p2, &ppref = pp;

    std:: cout << std::endl << std::endl;

    pref.doEverything();
    p2ref.doEverything();
    ppref.doEverything();

    std::vector<Bird*> birds;
    birds.push_back(&p);
    birds.push_back(&p2);
    birds.push_back(&pp);

    for (const auto &bird : birds ) {
        bird->doEverything();
    }

    try {
        BirdFactory::makeBird(static_cast<BirdType>(2));
    } catch ( const std::string &e ) {
        std::cerr << "EXCEPTION " << e << std::endl;
        return 0;
    }



    return 0;
}