#include <iostream>
#include "bird.h"

size_t Pidgeon::counter = 0;
size_t Penguin::counter = 0;
size_t Duck::counter = 0;

void Pidgeon::doEverything() const {
    this->jump();
    this->make_noise();
    this->flap_wings();
    this->fly();
}

void Penguin::doEverything() const {
    this->jump();
    this->make_noise();
    this->flap_wings();
    this->swim();
}

void Bird::jump() const {
    std::cout << this->name << " " << this->id << " can jump" << std::endl;
}

void Bird::make_noise() const {
    std::cout << this->name << " " << this->id << " makes noise" << std::endl;
}

void Bird::flap_wings() const {
    std::cout << this->name << " " << this->id << " flaps its wings" << std::endl;
}

void FlyingBird::fly() const {
    std::cout << this->name << " " << this->id << " is flying" << std::endl;
}

void SwimmingBird::swim() const {
    std::cout << this->name << " " << this->id << " is swimming" << std::endl;
}

Bird *BirdFactory::makeBird(BirdType type) {

    switch ( type ) {
        case t_PIDGEON:
            return new Pidgeon;

        case t_PENGUIN:
            return new Penguin;

        case t_DUCK:
            return new Duck;

        default:
            throw std::string("Invalid bird");
    }

}

void Duck::doEverything() const {
    this->jump();
    this->make_noise();
    this->flap_wings();
    this->swim();
    this->fly();
}
