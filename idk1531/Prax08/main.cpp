#include <iostream>
#include <vector>
#include "bird.h"

int main() {

    // cannot instantiate Bird
    // Bird b("bird",0);

    // cannot instantiate FlyingBird
    // FlyingBird("bird",0);

    // cannot instantiate SwimmingBird
    // SwimmingBird("bird",0);

    std::vector<Bird*> birds;

    try {
        birds.push_back(BirdFactory::makeBird(t_PIDGEON));
        birds.push_back(BirdFactory::makeBird(t_PENGUIN));
        birds.push_back(BirdFactory::makeBird(t_DUCK));
        birds.push_back(BirdFactory::makeBird(t_DUCK));
        birds.push_back(BirdFactory::makeBird(t_DUCK));
        BirdFactory::makeBird(t_OSTRICH);
    } catch ( const std::string &e ) {
        std::cerr << "EXCEPTION " << e << std::endl;
    }

    for (const auto &bird : birds ) {
        bird->doEverything();
    }

    return 0;
}