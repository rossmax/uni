#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <functional>
#include <queue>
#include <random>
#include <chrono>
#include <list>
#include <stack>

struct Person {
    std::string name;
    size_t age;
    friend bool operator< ( const Person &lhs, const Person &rhs ) {
        return lhs.name < rhs.name;
    }
    friend bool operator> ( const Person &lhs, const Person &rhs ) {
        return lhs.name > rhs.name;
    }
    friend std::ostream& operator<< ( std::ostream &os, const Person &p ) {
        return os << p.name << ", " << p.age;
    }
};

struct PersonAgeComparator {
    bool operator() ( const Person &lhs, const Person &rhs ) {
        return lhs.age < rhs.age;
    }
};

bool ageComparator( const Person &lhs, const Person &rhs ) {
    return lhs.age < rhs.age;
}

int main() {

    std::pair<std::string,int> alice("alice",5), bob = std::make_pair("bob",2);
    std::cout << alice.first << "\t" << alice.second << std::endl;
    std::cout << bob.first << "\t" << bob.second << std::endl;

    std::tuple<std::string,int,int,int> t1("alice",2,3,4), t2 = std::make_tuple("bob",4,5,4);

    std::string name; int result1, result2, result3;
    name = std::get<0>(t1);
    result1 = std::get<1>(t1);

    std::tie(name, result1, result2, result3) = t1;
    std::cout << name << "\t" << result1 << "\t" << result2 << "\t" << result3 << std::endl;

    std::vector<std::tuple<std::string,int,int,int>> results;
    // results.push_back(std::make_tuple("alice",2,3,4));
    results.emplace_back("carol",5,4,5);
    results.emplace_back("bob",3,4,5);
    results.emplace_back("alice",2,3,4);
    results.emplace_back("eve",3,4,4);

    std::cout << "Results \t";
    for ( auto const &t : results ) {
        std::cout << std::get<0>(t) << "\t";
    }
    std::cout << std::endl;

    std::priority_queue<int> q1;
    std::priority_queue<int, std::vector<int>, std::greater<int>> q2;
    std::priority_queue<Person,std::vector<Person>, PersonAgeComparator> persons;

    std::default_random_engine generator (std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<int> distribution(1,100);
    auto random = std::bind(distribution,generator);

    for ( auto i=0; i<10; i++ ) {
        q2.push(random());
    }
    std::cout << "Biggest element " << q2.top() << std::endl;

    Person p_alice {"alice",50};  persons.push(p_alice);
    Person p_bob {"bob",40};      persons.push(p_bob);
    Person p_carol {"carol",30};  persons.push(p_carol);
    Person p_eve {"eve",20};      persons.push(p_eve);

    while ( !persons.empty() ) {
        std::cout << "Person " << persons.top() << std::endl;
        persons.pop();
    }

    std::queue<int> queue1;
    std::queue<int,std::list<int>> queue2;

    for ( auto i=0; i<10; i++ ) {
        i % 2 ? queue1.push(i) : queue2.push(i);
    }

    std::cout << "Queue 1: ";
    while ( !queue1.empty() ) {
        std::cout << queue1.front() << "\t";
        queue1.pop();
    }
    std::cout << std::endl;

    std::cout << "Queue 2: ";
    while ( !queue2.empty() ) {
        std::cout << queue2.front() << "\t";
        queue2.pop();
    }
    std::cout << std::endl;


    std::stack<int> s1;
    std::stack<int,std::vector<int>> s2;

    for ( auto i=0; i<10; i++ ) {
        i % 2 ? s1.push(i) : s2.push(i);
    }

    std::cout << "S1: ";
    while ( !s1.empty() ) {
        std::cout << s1.top() << "\t";
        s1.pop();
    }
    std::cout << std::endl;

    std::cout << "S2: ";
    while ( !s2.empty() ) {
        std::cout << s2.top() << "\t";
        s2.pop();
    }
    std::cout << std::endl;

    std::vector<Person> people;
    people.push_back({"alice",35});
    people.push_back({"bob",22});
    people.push_back({"carol",33});
    people.push_back({"eve",44});

    std::sort(people.begin(),people.end(),PersonAgeComparator());
    std::sort(people.begin(),people.end(),ageComparator);

    for ( auto const &person : people ) {
        std::cout << person << std::endl;
    }

    return 0;
}