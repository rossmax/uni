#include <iostream>
#include "item.h"
#include "order.h"
#include "stock.h"

int main() {

    Item i1("Bread", 0.95);
    Item i2("Milk", 0.55);

    std::cout << i1 << std::endl << i2 << std::endl;

    Order receipt, receipt2;

    try {
        receipt.add(i1, 1);
        receipt.add(i2, 5);
        receipt2.add(i1,3);
    } catch ( const std::exception &e ) {
        std::cerr << "[EXCEPTION]: " << e.what() << std::endl;
    }

    for ( auto element : receipt ) {
        std::cout << element.first << " qty " << element.second << " pcs." << std::endl;
    }

    std::cout << receipt << std::endl;

    Stock s;
    s.add(receipt);
    s.add(receipt2);
    std::cout << "Bread items " << s.getAvailable(i1) << std::endl;
    std::cout << "Milk items " << s.getAvailable(i2) << std::endl;

    Order shipment;
    shipment.add(i2,5);
    shipment.add(i1,2);

    s.dispatch(shipment);

    std::cout << "Bread items " << s.getAvailable(i1) << std::endl;
    std::cout << "Milk items " << s.getAvailable(i2) << std::endl;

    return 0;
}