#ifndef PRAX10_ORDER_H
#define PRAX10_ORDER_H

#include <map>
#include "item.h"

class Order {
public:
    void add(const Item &item, size_t qty);
    std::map<Item,size_t>::iterator begin();
    std::map<Item,size_t>::iterator end();
    std::map<Item,size_t>::const_iterator cbegin() const;
    std::map<Item,size_t>::const_iterator cend() const;
    friend std::ostream& operator<< ( std::ostream &os, const Order &order );
private:
    std::map<Item,size_t> items;
};

#endif //PRAX10_ORDER_H
