#include <iostream>
#include <sstream>
#include "stock.h"

void Stock::add(const Order &order) {

    for ( auto it=order.cbegin(); it != order.cend(); it++ ) {
        const Item &item = it->first;
        const size_t &qty = it->second;
        items[item] += qty;

        // items[it->first] += it->second;
    }

}

void Stock::dispatch(const Order &order) {

    for ( auto it=order.cbegin(); it != order.cend(); it++ ) {
        const Item &requestedItem = it->first;
        const size_t &requestedQty = it->second;
        const size_t &availableQty = items[requestedItem];

        if ( availableQty < requestedQty ) {
            std::stringstream ss;
            ss << "Item " << requestedItem << " is not of enough quantity.";
            throw std::invalid_argument(ss.str());
        }

        items[requestedItem] -= requestedQty;

        if ( items[requestedItem] == 0 ) {
            items.erase(requestedItem);
        }
    }

}

size_t Stock::getAvailable(const Item &item) {
    return items[item];
}
