
#ifndef PRAX10_STOCK_H
#define PRAX10_STOCK_H

#include <map>
#include "item.h"
#include "order.h"

class Stock {
public:
    void add(const Order &order);
    size_t getAvailable(const Item &item);
    void dispatch(const Order &order);
private:
    std::map<Item,size_t> items;
};

#endif //PRAX10_STOCK_H
