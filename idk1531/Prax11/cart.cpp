#include <algorithm>
#include <iostream>
#include <numeric>
#include "cart.h"

void Cart::add(Item item, size_t qty) {
    if ( qty == 0 ) {
        throw std::invalid_argument("Quantity cannot be zero.");
    }

    items[item] += qty;
}

void Cart::change(Item item, size_t qty) {
    if ( items.count(item) == 0 ) {
        throw std::invalid_argument("Item not present in shopping cart");
    }

    items[item] = qty;
}

void Cart::remove(Item item) {
    if ( items.count(item) == 0 ) {
        throw std::invalid_argument("Item not present in shopping cart");
    }

    items.erase(item);
}

std::ostream &operator<<(std::ostream &os, const Cart &cart) {
    for ( auto entry : cart.items ) {
        os << entry.first << " qty " << entry.second << " pcs" << std::endl;
    }
    return os;
}

void Cart::clear() {
    items.clear();
    //items.erase(items.begin(), items.end());
}

float Cart::getTotal1() {
    float price = 0;

    auto lambda = [&price] (std::pair<Item,size_t> next) {
        price += next.first.getPrice() * next.second;
    };

    std::for_each(items.begin(), items.end(), lambda);

    return price;
}

float Cart::getTotal2() {

    auto lambda = [] (float total, std::pair<Item,size_t> itemPair) {
        return total + itemPair.first.getPrice() * itemPair.second;
    };

    return std::accumulate(items.begin(), items.end(), 0.0f, lambda);
}

Order Cart::checkOut() {
    Order o;
    for ( auto itemPair : items ) {
        o.add(itemPair.first,itemPair.second);
    }
    return o;
}
