#ifndef PRAX11_CART_H
#define PRAX11_CART_H

#include <map>
#include "item.h"
#include "order.h"

class Cart {
public:
    void add(Item item, size_t qty = 1);
    void change(Item item, size_t qty);
    void remove(Item item);
    void clear();
    float getTotal1();
    float getTotal2();
    friend std::ostream &operator<< (std::ostream &os, const Cart &cart);
    Order checkOut();
private:
    std::map<Item,size_t> items;
};

#endif //PRAX11_CART_H
