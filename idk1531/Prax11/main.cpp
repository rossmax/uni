#include <iostream>
#include "item.h"
#include "order.h"
#include "stock.h"
#include "cart.h"

int main() {

    Item i1("Bread", 0.95);
    Item i2("Milk", 0.55);

    std::cout << i1 << std::endl << i2 << std::endl;

    Order receipt, receipt2;

    try {
        receipt.add(i1, 1);
        receipt.add(i2, 5);
        receipt2.add(i1,3);
    } catch ( const std::exception &e ) {
        std::cerr << "[EXCEPTION]: " << e.what() << std::endl;
    }

    for ( auto element : receipt ) {
        std::cout << element.first << " qty " << element.second << " pcs." << std::endl;
    }

    std::cout << receipt << std::endl;

    Stock s;
    s.add(receipt);
    s.add(receipt2);
    std::cout << "Bread items " << s.getAvailable(i1) << std::endl;
    std::cout << "Milk items " << s.getAvailable(i2) << std::endl;

    Order shipment;
    shipment.add(i2,5);
    shipment.add(i1,2);

    s.dispatch(shipment);

    std::cout << "Bread items " << s.getAvailable(i1) << std::endl;
    std::cout << "Milk items " << s.getAvailable(i2) << std::endl;

    try {
        std::cout << "STORE: " << std::endl << s << std::endl;
        Cart c;
        c.add(i1);
        c.add(i2,2);
        c.change(i1,3);
        c.remove(i2);
        c.add(i1,1);
        c.add(i1,1);
        std::cout << c << std::endl;
        std::cout << "Total price: " << c.getTotal1() << std::endl;
        std::cout << "Total price: " << c.getTotal2() << std::endl;

        c.clear();
        c.add(i1,2);

        Order shipment2 = c.checkOut();
        s.dispatch(shipment2);

        auto transactions = s.getAllTransactions();
        std::cout << "Transactions " << transactions.size() << std::endl;

        auto breadTransactions1 = s.getItemTransactions1(i1);
        auto milkTransactions1 = s.getItemTransactions1(i2);
        auto breadTransactions2 = s.getItemTransactions2(i1);
        auto milkTransactions2 = s.getItemTransactions2(i2);
        std::cout << "Bread transactions " << breadTransactions1.size() << std::endl;
        std::cout << "Milk transactions " << milkTransactions1.size() << std::endl;
        std::cout << "Bread transactions " << breadTransactions2.size() << std::endl;
        std::cout << "Milk transactions " << milkTransactions2.size() << std::endl;
    } catch ( const std::exception &e ) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}