#include "order.h"


void Order::add(const Item &item, size_t qty) {

    if ( qty == 0 ) {
        throw std::invalid_argument("Item quantity must be positive");
    }

    items[item] += qty;

}

std::map<Item,size_t>::iterator Order::begin() {
    return items.begin();
}

std::map<Item,size_t>::iterator Order::end() {
    return items.end();
}

std::map<Item,size_t>::const_iterator Order::begin() const {
    return items.cbegin();
}

std::map<Item,size_t>::const_iterator Order::end() const {
    return items.cend();
}

std::ostream &operator<<(std::ostream &os, const Order &order) {

    for ( auto it = order.begin(); it != order.end(); it++ ) {
        // Item &item = it->first;
        // size_t &qty = it->second;
        os << it->first << " qty " << it->second << " pcs." << std::endl;
    }

    return os;
}

