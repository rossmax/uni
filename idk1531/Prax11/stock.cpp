#include <iostream>
#include <sstream>
#include <tuple>
#include <algorithm>
#include <numeric>
#include "stock.h"

void Stock::add(const Order &order) {

    for ( auto it=order.begin(); it != order.end(); it++ ) {
        const Item &item = it->first;
        const size_t &qty = it->second;
        items[item] += qty;

        // items[it->first] += it->second;

        transactions.emplace(item,qty,order);
    }

}

void Stock::dispatch(const Order &order) {

    for ( auto it=order.begin(); it != order.end(); it++ ) {
        const Item &requestedItem = it->first;
        const size_t &requestedQty = it->second;
        const size_t &availableQty = items[requestedItem];

        if ( availableQty < requestedQty ) {
            std::stringstream ss;
            ss << "Item " << requestedItem << " is not of enough quantity.";
            throw std::invalid_argument(ss.str());
        }

        items[requestedItem] -= requestedQty;
        // transactions.insert(std::tuple<Item,int,Order>(requestedItem,-requestedQty,order));
        transactions.emplace(requestedItem,-requestedQty,order);

        if ( items[requestedItem] == 0 ) {
            items.erase(requestedItem);
        }
    }

}

std::ostream &operator<<(std::ostream &os, const Stock &s) {
    for ( auto itemPair : s.items ) {
        os << itemPair.first << " qty " << itemPair.second << " pcs" << std::endl;
    }
    return os;
}

size_t Stock::getAvailable(const Item &item) {
    return items[item];
}

std::multiset<Transaction, TransactionComparator> Stock::getAllTransactions() {
    return transactions;
}

std::vector<Transaction> Stock::getItemTransactions1(const Item &item) {
    std::vector<Transaction> output;

    std::for_each(transactions.begin(), transactions.end(),
          [&output,item](const Transaction &t) {
              if ( std::get<0>(t) == item ) {
                  output.push_back(t);
              }
          }
    );

    return output;
}

std::vector<Transaction> Stock::getItemTransactions2(const Item &item) {
    return std::accumulate(transactions.begin(), transactions.end(),
        std::vector<Transaction>(),
        [item] (std::vector<Transaction> total, const Transaction &t) {
            if ( std::get<0>(t) == item ) {
                total.push_back(t);
            }
            return total;
        }
    );
}
