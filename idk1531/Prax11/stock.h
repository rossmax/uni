
#ifndef PRAX10_STOCK_H
#define PRAX10_STOCK_H

#include <map>
#include <set>
#include <vector>
#include "item.h"
#include "order.h"

using Transaction = std::tuple<Item,int,Order>;

struct TransactionComparator {
    bool operator() ( const Transaction &lhs, const Transaction &rhs ) {
        const Item &item1 = std::get<0>(lhs);
        const Item &item2 = std::get<0>(rhs);
        return item1.getPrice() < item2.getPrice();
    }
};

class Stock {
public:
    void add(const Order &order);
    size_t getAvailable(const Item &item);
    void dispatch(const Order &order);
    friend std::ostream &operator<< ( std::ostream &os, const Stock &s );
    std::multiset<Transaction,TransactionComparator> getAllTransactions();
    std::vector<Transaction> getItemTransactions1(const Item &item);
    std::vector<Transaction> getItemTransactions2(const Item &item);
private:
    std::map<Item,size_t> items;
    std::multiset<Transaction,TransactionComparator> transactions;
};

#endif //PRAX10_STOCK_H
