cmake_minimum_required(VERSION 3.8)
project(Prax12)

set(CMAKE_CXX_STANDARD 17)

find_package(Threads)

set(SOURCE_FILES main.cpp board.cpp board.h tile.cpp tile.h player.cpp player.h)
add_executable(Prax12 ${SOURCE_FILES})
target_link_libraries(Prax12 Threads::Threads)