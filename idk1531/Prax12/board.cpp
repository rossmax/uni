#include <iostream>
#include "board.h"

Board::Board() {
    start = last = nullptr;
}

std::shared_ptr<Tile> Board::getStartTile() {
    if ( start == nullptr ) {
        throw std::runtime_error("Start tile is not set");
    }
    return start;
}

Board &Board::append(std::shared_ptr<Tile> &&t) {

    if ( start == nullptr ) {
        last = start = std::move(t);
    } else {
        last->next = std::move(t);
        last->next->prev = last;
        last = last->next;
    }
    return *this;
}
