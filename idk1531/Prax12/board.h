#ifndef PRAX12_BOARD_H
#define PRAX12_BOARD_H

#include "tile.h"

class Board {
public:
    Board();
    std::shared_ptr<Tile> getStartTile();
    Board &append(std::shared_ptr<Tile> &&t);
private:
    std::shared_ptr<Tile> start, last;
};

#endif //PRAX12_BOARD_H
