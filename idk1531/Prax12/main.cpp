#include <iostream>
#include <thread>
#include <vector>
#include "board.h"
#include "player.h"

int main() {

    Board b;
    auto t1 = std::make_shared<Tile>("Narrow path");
    auto t2 = std::make_shared<Tile>("Old tree");
    auto t3 = std::make_shared<Tile>("Rabbit hole");
    auto t4 = std::make_shared<Tile>("Bottom of rabbit hole");
    try {
        b.append(move(t1))
                .append(move(t2))
                .append(move(t3))
                .append(move(t4))
                ;
    } catch ( const std::exception &e ) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    std::unique_ptr<Player> alice;
    alice = std::make_unique<Player>("Alice", b.getStartTile());
    alice->forward();

   /* std::vector<std::thread> threads;
    for ( auto &s : { "Alice", "Bob", "Carol", "Eve", "Peter" } ) {
        threads.emplace_back(Player(s,b.getStartTile()));
    }

    for ( auto &t : threads ) {
        if ( t.joinable() ) {
            t.join();
        }
    }

    return 0;*/
}