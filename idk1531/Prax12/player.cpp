#include <sstream>
#include <iostream>
#include <thread>
#include "player.h"

Player::Player(const std::string &playerName, std::shared_ptr<Tile> &&startTile) {
    name = playerName;
    current = start = std::move(startTile);
    coins = 0;
    current->enter(this);
}

std::ostream &operator<<(std::ostream &os, const Player &player) {
    return os << player.name;
}

void Player::forward() {
    current->leave(this);
    if ( current->next == nullptr ) {
        std::stringstream ss;
        ss << *current << " is the last tile in the sequene. Cannot move forward.";
        throw std::runtime_error(ss.str());
    }
    current = current->next;
    current->enter(this);
}

void Player::backward() {
    current->leave(this);
    if ( current->prev == nullptr ) {
        std::stringstream ss;
        ss << *current << " is the first tile in the sequence. Cannot move backward.";
        throw std::runtime_error(ss.str());
    }
    current = current->prev;
    current->enter(this);
}

void Player::restart() {
    current->leave(this);
    current = start;
    current->enter(this);
}

void Player::operator()() {
    try {
        for(auto i=0; i<10; i++) {
            std::this_thread::sleep_for(
                std::chrono::milliseconds(250)
            );
            forward();
        }
    } catch ( const std::exception &e ) {
        std::cerr << e.what() << std::endl;
    }
}
