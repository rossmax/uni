#ifndef PRAX12_PLAYER_H
#define PRAX12_PLAYER_H

#include <string>
#include "tile.h"

class Player {
    friend class LotteryTile;
    friend class HotelTile;
    friend class GhettoTile;
public:
    Player(const std::string &playerName, std::shared_ptr<Tile> &&startTile);
    friend std::ostream &operator<< ( std::ostream &os, const Player &player );
    void forward();
    void backward();
    void restart();
    void operator()();
private:
    std::string name;
    size_t coins;
    std::shared_ptr<Tile> start, current;
};

#endif //PRAX12_PLAYER_H
