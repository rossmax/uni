#include <iostream>
#include <random>
#include <chrono>
#include <sstream>
#include "tile.h"
#include "player.h"

Tile::Tile(const std::string &tileName) {
    name = tileName;
    prev = next = nullptr;
}

std::ostream &operator<<(std::ostream &os, const Tile &tile) {
    return os << "Tile " << tile.name;
}

void LotteryTile::enter(Player *player) {
    std::default_random_engine generator(
            std::chrono::high_resolution_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<int> distribution(20,50);
    auto giveMeMoney = std::bind(distribution,generator);
    auto coins = giveMeMoney();
    player->coins += coins;

    std::cout << *player << " entered lottery tile " << name
              << " and wins " << coins << " coins" << std::endl;
}

void LotteryTile::leave(Player *player) {
    std::cout << *player << " left lottery tile " << name << std::endl;
}

void HotelTile::enter(Player *player) {
    if ( player->coins < 12 ) {
        std::stringstream ss;
        ss << *player << " does not have enough coins to pay for hotel";
        throw std::runtime_error(ss.str());
    }

    player->coins -= 12;
    std::cout << *player << " entered hotel tile and paid 12 coins" << std::endl;
}



void HotelTile::leave(Player *player) {
    std::cout << *player << " left hotel tile" << std::endl;
}

void GhettoTile::enter(Player *player) {
    player->coins /= 2;
    std::cout << *player << " entered a ghetto tile, got robbed and lost "
              << player->coins << " coins" << std::endl;
}

void GhettoTile::leave(Player *player) {
    std::cout << *player << " left ghetto tile" << std::endl;
}

void RestartTile::enter(Player *player) {
    std::cout << *player << " entered restart tile" << std::endl;
    player->restart();
}

void RestartTile::leave(Player *player) {
    std::cout << *player << " left restart tile" << std::endl;
}

void Tile::enter(Player *player) {
    std::cout << *player << " entered tile " << name << std::endl;
}

void Tile::leave(Player *player) {
    std::cout << *player << " left tile " << name << std::endl;
}