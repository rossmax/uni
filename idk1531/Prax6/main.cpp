#include <iostream>
#include <memory>

using NodeHandler = int (*) (int,int);

class TreeNode {
public:
    /**
     * Constructor for a leaf
     * @param v value of the leaf
     */
    explicit TreeNode(int v) : op(nullptr) {
        value = v;
    }

    /**
     * Constructor for an operator
     * @param handler pointer to a function with signature int (int,int)
     */
    explicit TreeNode(NodeHandler handler) {
        value = 0;
        op = handler;
    }

    /**
     * Checks if a node is operator
     * @return true if a node is an operator, false otherwise
     */
    bool isOperator() {
        return op != nullptr;
    }

    /**
     * Checks if a node is a leaf
     * @return true if a node is a leaf, false otherwise
     */
    bool isLeaf() {
        return op == nullptr;
    }

    /**
     * Set the left-hand-side child of the node
     * @param child a pointer to a TreeNode type object
     */
    void setLeftChild(TreeNode *child) {
        left.reset(child);
    }

    /**
     * Set the right-hand-side child of the node
     * @param child a pointer to a TreeNode type object
     */
    void setRightChild(TreeNode *child) {
        right.reset(child);
    }

    /**
     * Evaluate the expression encoded in the tree.
     * A recursive procedure that returns values for leafs,
     * and for operator nodes it applies the stored function
     * to node children and returns the result of this call.
     */
    int evaluateExpression() {

        if ( isLeaf() ) {
            return value;
        }

        int leftChildValue = left->evaluateExpression();

        int rightChildValue = right->evaluateExpression();
        int opVal = op(leftChildValue,rightChildValue);
        return opVal;

    }

    /**
     * Returns the value for the node
     * @return value (am integer)
     */
    int getValue() {
        return value;
    }
private:
    int value;
    NodeHandler op;
    std::unique_ptr <TreeNode> left, right;
};

int mul(int a, int b) {
    return a * b;
}

int add(int a, int b) {
    return a + b;
}

int main() {

    auto *prod = new TreeNode(mul);
    prod->setLeftChild(new TreeNode(2));
    prod->setRightChild(new TreeNode(3));

    auto *sum = new TreeNode(add);
    sum->setLeftChild(new TreeNode(4));
    sum->setRightChild(new TreeNode(5));

    auto *root = new TreeNode(add);
    root->setLeftChild(prod);
    root->setRightChild(sum);

    std::cout << "4 + 5 = " << sum->evaluateExpression() << std::endl;
    std::cout << "Root node evaluation " << root->evaluateExpression() << std::endl;



    return 0;
}