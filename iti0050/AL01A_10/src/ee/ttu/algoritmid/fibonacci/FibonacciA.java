package ee.ttu.algoritmid.fibonacci;

import java.math.BigInteger;

public class FibonacciA {

    /**
     * Compute the Fibonacci sequence number.
     * @param n The number of the sequence to compute.
     * @return The n-th number in Fibonacci series
     */
    public static void main(String[] args){
        System.out.println(iterativeF(3));
    }

    public static String iterativeF(int n) {

        if(n < 1) {
            return "0";
        }
        if(n == 1) {
            return "1";
        }
        BigInteger fib = BigInteger.valueOf(1);
        BigInteger prevFib = BigInteger.valueOf(1);

        for(long i=2; i<n; i++) {
            BigInteger temp = fib;
            fib = fib.add(prevFib);
            prevFib = temp;
        }

        return String.valueOf(fib);
    }



}