package ee.ttu.algoritmid.fibonacci;

import java.math.BigInteger;
import java.util.Calendar;

public class FibonacciB_2 {

    /**
     * Estimate or find the exact time required to compute the n-th Fibonacci number.
     * @param n The n-th number to compute.
     * @return The time estimate or exact time in YEARS.
     */

    public static void main(String[] args){

        // System.out.println(recursiveF(40));
        System.out.println(timeToComputeRecursiveFibonacci(40));
    }

    public static String timeToComputeRecursiveFibonacci(int n) {

        long startTime = System.currentTimeMillis();
        recursiveF(n);
        long endTime = System.currentTimeMillis();
        long dif = endTime - startTime;
        float res = (float)dif;
        res = res/1000/60/60/24/365;
        return String.format("%.18f" , res);
    }

    /**
     * Compute the Fibonacci sequence number recursively.
     * (You need this in the timeToComputeRecursiveFibonacci(int n) function!)
     * @param n The n-th number to compute.
     * @return The n-th Fibonacci number as a string.
     */
    public static BigInteger recursiveF(int n) {
        if (n <= 1)
            return BigInteger.valueOf(n);
        return recursiveF(n - 1).add(recursiveF(n - 2));
    }

}