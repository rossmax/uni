package ee.ttu.algoritmid.guessinggame;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class GuessingGame {

    Oracle oracle;

    public GuessingGame(Oracle oracle) {
        this.oracle = oracle;
    }

    public static void main(String[]args){
        Student one1 =  new Student("John",174);
        Student one2 =  new Student("Max",180);
        Student one3 =  new Student("Andrew",171);
        Student one4 =  new Student("Lucas",179);
        Student one5 =  new Student("Bob",190);
        Student one6 =  new Student("Mathew",186);
        Student one7 =  new Student("Lidia",178);

        Oracle or = new Oracle(one7);

        Student[] myStringArray = new Student[]{one1,one2,one3,one4,one5,one6,one7};

        GuessingGame example = new GuessingGame(or);

        System.out.println(example.play(myStringArray));
    }

    /**
     * @param studentArray - All the possible students.
     * @return the name of the student
     */
    public String play(Student[] studentArray) {

        Student one = new Student("Undefined",0);
        String answer = "";

        Arrays.sort(studentArray,(Student s1, Student s2) -> s1.getHeight() - s2.getHeight());

        int low = 0;
        int high = studentArray.length - 1;

        while (high >= low) {
            int mid = (low + high) / 2;
            answer = oracle.isIt(studentArray[mid]);
            if(answer.equals("correct!")) {
                return studentArray[mid].getName();
            }
            if(answer.equals("taller")) {
                low = mid + 1;
            }
            if(answer.equals("shorter")) {
                high = mid - 1;
            }
        }
        return one.getName();
    }
}