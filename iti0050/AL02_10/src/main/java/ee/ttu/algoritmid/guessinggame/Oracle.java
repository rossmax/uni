package ee.ttu.algoritmid.guessinggame;

public class Oracle {

    private Student student;

    public Oracle(Student student) {
        this.student = student;
    }

    /**
     * @param studentGuess - student you think is correct
     * @return
     *     "taller" if correct student is taller than your guess
     *     "shorter" if correct student is shorter than your guess
     *     "correct!" if your guess is correct
     */
    public  String isIt(Student studentGuess) {
        if (this.student.getHeight() > studentGuess.getHeight()) {
            return "taller";
        } else if (this.student.getHeight() < studentGuess.getHeight()) {
            return "shorter";
        }
        return "correct!";
    }
}
