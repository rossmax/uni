package ee.ttu.algoritmid.guessinggame;

import java.util.Comparator;

public class Student {

    private String name;
    private int height;

    public  Student(String name, int height) {
        this.name = name;
        this.height = height;
    }

    public  String getName() {
        return this.name;
    }

    public  int getHeight() {
        return this.height;
    }
}