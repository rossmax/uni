package ee.ttu.algoritmid.queue;

public class Queue {

    // Don't change those lines
    final Stack stack1;
    final Stack stack2;

    public Queue() {
        this.stack1 = new Stack();
        this.stack2 = new Stack();
    }

    public void enqueue(int number) {

        stack1.push(number);
    }

    public int dequeue() {
        if(stack2.isEmpty()) {
            while(!stack1.isEmpty()){
                stack2.push(stack1.pop());
            }
        }
        return stack2.pop();
    }

    public boolean isEmpty() {
        return stack1.isEmpty() && stack2.isEmpty();
    }

    public static void main(String[]args) {
        Queue Qone =  new Queue();

        Stack Sone = new Stack();

        Qone.enqueue(3);

        Qone.enqueue(5);

        Qone.enqueue(2);

        System.out.println(Qone.dequeue());
        System.out.println(Qone.dequeue());
        System.out.println(Qone.dequeue());
        Qone.enqueue(6);
        System.out.println(Qone.dequeue());
        ///xx
    }
}