package ee.ttu.algoritmid.queue;

public class Deque {

    // Don't change those lines
    Stack stack1;
    Stack stack2;

    public Deque() {
        this.stack1 = new Stack();
        this.stack2 = new Stack();
    }

    public void pushFirst(int number) {
        if(stack1.isEmpty() && stack2.isEmpty()) stack1.push(number);
        if(!stack1.isEmpty() && stack2.isEmpty()) stack1.push(number);
        if(stack1.isEmpty() && !stack2.isEmpty()) stack2.push(number);
    }

    public void pushLast(int number) {
        if(stack2.isEmpty()) {
            while(!stack1.isEmpty()){
                stack2.push(stack1.pop());
            }
            stack2.push(number);
        }

        if(stack1.isEmpty()) {
            while(!stack1.isEmpty()){
                stack2.push(stack1.pop());
            }
            stack2.push(number);
        }

    }

    public int popFirst() {
        if(!stack1.isEmpty())return stack1.pop();
        else return stack2.pop();
    }

    public int popLast() {
        if (stack2.isEmpty()) {
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
        }
        else if if (stack2.isEmpty()) {
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
        }
        return stack2.pop();
    }
    public boolean isEmpty() {
        return stack1.isEmpty() && stack2.isEmpty();
    }

    public int popMin() {
        // TODO: Remove the least significant number from the deque.
        return 0;
    }

    public void reverse() {
        if (stack2.isEmpty()) {
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
        }
        if (stack1.isEmpty()) {
            while (!stack2.isEmpty()) {
                stack1.push(stack2.pop());
            }
        }
    }
    public static void main(String[]args) {
        Deque Qone =  new Deque();
        Stack Sone = new Stack();

        System.out.println(Qone.isEmpty());
        Qone.pushFirst(3);
        System.out.println(Qone.isEmpty());
        Qone.pushFirst(5);
        Qone.pushLast(2);
        Qone.pushLast(4);
        Qone.reverse();
        //Qone.popMin();
        Qone.popFirst();
        Qone.popLast();
        System.out.println(Qone.isEmpty());

    }
}
