package ee.ttu.algoritmid.tsp;

import java.util.Arrays;

/**
 * Created by rossm on 11/12/2017.
 */
public class GreedyTSP {

    public static void main(String[] args) {

        int[][] adjacencyMatrix = new int[][] {
                {0, 4, 1},
                {3, 0, 5},
                {2, 6, 0}
        };
        System.out.println(Arrays.toString(greedySolution(adjacencyMatrix)));
    }

    /* Greedy search */
    public static int[] greedySolution(int[][] adjacencyMatrix) {
        int size = adjacencyMatrix.length;
        boolean[] copy = new boolean[size];
        int[] shortestPath = new int[size];
        int current = 0;
        double bestDistance = Double.MAX_VALUE;

        // nearest neighbour thingy
        int town = current;
        for (int a = 0; a < size; a++) {
            // reset distance array
            Arrays.fill(copy, true);
            double shortest = 0, dist = 0;
            int[] temp = new int[size];
            int index = 0;
            temp[index++] = a + 1;
            current = a;

            for (int c = 0; c < size - 1; c++) {
                shortest = Double.MAX_VALUE; // reset closest

                for (int i = 0; i < size; i++) {
                    if (i == current) continue;
                    if (copy[i] && (adjacencyMatrix[current][i] < shortest)) {
                        town = i;
                        shortest = adjacencyMatrix[current][i];
                    }
                }

                copy[current] = false;
                temp[index++] = town + 1;
                current = town;
                dist += shortest;
            }

            dist += adjacencyMatrix[temp[index - 1] - 1][temp[0] - 1];
            if (dist < bestDistance) {
                shortestPath = Arrays.copyOf(temp, temp.length);
                bestDistance = dist;
            }
        }
        return shortestPath;
    }
}

