package ee.ttu.algoritmid.dancers;

import java.util.ArrayList;

public class BinaryTree {


    public Node root = null;

    // find next larger element than n(smallest on the right)
    private Node findSuccessor(Node node) {
        Node y = null;

        if (node == null) return null;

        if (node.hasRight()) return findMinimum(node.right);

        if (node.hasParent()) {
            y = node.parent;
        }

        while (node == y.right && y != null) {
            node = y;
            y = y.parent;
        }
        return y;
    }

    // find next smaller element than n(biggest on the left)
    private Node findPredecessor(Node n) {
        Node parent = null;

        if (n.hasLeft()) return findMaximum(n.left);

        if (n.hasParent()) {
            parent = n.parent;
        }

        while (n == parent.left && parent != null) {
            n = parent;
            parent = parent.parent;
        }
        return parent;
    }

    private Node findMinimum(Node node) {
        if (node.hasLeft()) return findMinimum(node.left);
        else return node;
    }

    public Node findMaximum(Node node) {
        if (node.hasRight()) return findMaximum(node.right);
        else return node;
    }

    private Node search(int key, Node current) {
        if (current.key == key) return current;

        if (current.key < key && current.hasRight()) return search(key, current.right);

        if (current.key > key && current.hasLeft()) return search(key, current.left);
        return null;
    }

    public Node search(int key) {
        return search(key, this.root);
    }

    public void insertNode(Dancer d) {
        Node dancer = new Node(d);

        if (root == null) {
            this.root = dancer;
            return;
        }

        Node x = this.root;
        Node y = null;

        while (x != null) {
            y = x;
            if (dancer.key < x.key) x = x.left;
            else x = x.right;
        }

        dancer.parent = y;

        if (y == null) this.root = dancer;
        else {
            if (dancer.key < y.key) y.left = dancer;
            else y.right = dancer;
        }
    }


    public int genKey(Dancer d) {
        if (d.getGender().name().equals("MALE")) {
            return Integer.parseInt(d.getHeight() + "0" + d.getID());
        } else {
            return Integer.parseInt(d.getHeight() + "1" + d.getID());
        }
    }

    public void remove(Dancer d) {
        Node s = search(genKey(d));
        Node y = null;
        Node x = null;

        if (!s.hasLeft() || !s.hasRight()) y = s;
        else y = findSuccessor(s);

        if (y.hasLeft()) x = y.left;
        else x = y.right;

        if (x != null) x.parent = y.parent;

        if (!y.hasParent()) this.root = x;
        else if (y == y.parent.left) y.parent.left = x;
        else y.parent.right = x;

        if (y != s) {
            s.dancer = y.dancer;
            s.key = y.key;
        }
        y = null;
    }

    // find female partner for male
    public Dancer findFemale(Dancer male) {
        int currMaleKey = genKey(male);

        Node current = search(currMaleKey);
        Node partner = findPredecessor(current);

        while (partner.isMale()) {
            partner = findPredecessor(partner);
        }
        return partner.dancer;
    }


    // find male partner for female
    public Dancer findMale(Dancer female) {
        int currFemaleKey = genKey(female);

        Node current = search(currFemaleKey);
        Node partner = findSuccessor(current);

        while (!partner.isMale()) {
            partner = findSuccessor(partner);
        }
        return partner.dancer;
    }

    // queue of dancers without a partner, sort by key
    public ArrayList<Dancer> sort(Node root, ArrayList<Dancer> output) {
        if (root.hasLeft()) sort(root.left, output);

        output.add(root.dancer);

        if (root.hasRight()) sort(root.right, output);

        return output;
    }
}