package ee.ttu.algoritmid.dancers;

/**
 * Created by rossm on 10/16/2017.
 */
/**
 * API specification for the objects
 * representing various dancers.
 */
public interface Dancer {
    public enum Gender {
        MALE, FEMALE
    }

    public int getID();
    public Gender getGender();
    public int getHeight();
}