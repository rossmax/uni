package ee.ttu.algoritmid.dancers;

public class DancerImpl implements Dancer{
    int height;
    Dancer.Gender gender;
    int tekeoutheight;
    int id;

    @Override
    public int getID() {
        return this.height;
    }

    @Override
    public Dancer.Gender getGender() {
        return this.gender;
    }

    @Override
    public int getHeight() {
        return this.id;
    }



    public DancerImpl(int height, Gender gender,int id) {
        this.height = height;
        this.gender = gender;
        this.id = id;
    }

}
