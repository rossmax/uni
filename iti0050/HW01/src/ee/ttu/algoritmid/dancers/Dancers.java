package ee.ttu.algoritmid.dancers;

/**
 * Created by rossm on 10/16/2017.
 */

import java.util.List;
import java.util.AbstractMap.SimpleEntry;

/**
 * API specification for the
 * functional call to be tested.
 * IMPORTANT! You *HAVE* to implement
 * the class named HW01 implementing
 * this interface in your solution.
 */
public interface Dancers {
    public SimpleEntry<Dancer, Dancer> findPartnerFor(Dancer d) throws IllegalArgumentException;

    /*
     * Returns waiting list as a list (both men and women)
     * Ordered shortest --> longest
     * If man and woman are having the same height,
     * then ordering should be woman, man
     */
    public List<Dancer> returnWaitingList();
}