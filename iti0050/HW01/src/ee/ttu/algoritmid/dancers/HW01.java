package ee.ttu.algoritmid.dancers;

import java.util.ArrayList;
import java.util.List;
import java.util.AbstractMap.SimpleEntry;

/**
 * Created by rossm on 10/16/2017.
 */
public class HW01 implements Dancers {

    public BinaryTree tree = new BinaryTree();


    private boolean checkDancer(Dancer d) {
        if ((d instanceof Dancer) && (!(d.getID() < 0)) && (!(d.getHeight() < 0))) return true;
        else return false;
    }

    @Override
    public SimpleEntry<Dancer, Dancer> findPartnerFor(Dancer candidate) throws IllegalArgumentException {
        if (!checkDancer(candidate)) throw new IllegalArgumentException();

        Dancer partner = null;

        tree.insertNode(candidate);

        try {
            if (candidate.getGender().name().equals("MALE")) {
                partner = tree.findFemale(candidate);
            } else {
                partner = tree.findMale(candidate);
        }

        } catch (NullPointerException ex) {
            return null;
        }

        if (partner != null) {
            tree.remove(candidate);
            tree.remove(partner);
            if (candidate.getGender().name().equals("MALE")) return new SimpleEntry<Dancer, Dancer>(partner, candidate);
            else return new SimpleEntry<Dancer, Dancer>(candidate, partner);
        } return null;
    }

    @Override
    public List<Dancer> returnWaitingList() {
        try {
            ArrayList<Dancer> result = new ArrayList<Dancer>();
            tree.sort(tree.root, result);

            int n = result.size();
            Dancer temp = null;

            for(int i=0; i < n; i++){
                for(int j=1; j < (n-i); j++){
                    if(result.get(j - 1).getHeight() == result.get(j).getHeight()
                            && result.get(j - 1).getGender().name().equals("MALE") && result.get(j).getGender().name().equals("FEMALE")){
                        //swap elements
                        temp = result.get(j-1);
                        result.set(j-1,result.get(j));
                        result.set(j,temp);
                    }
                }
            }

            return result;

        } catch(NullPointerException n) {
            return new ArrayList<Dancer>();
        }
    }
}