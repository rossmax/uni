package ee.ttu.algoritmid.dancers;

/**
 * Created by rossm on 10/28/2017.
 */
public class Node {
    Dancer dancer = null;
    Node parent = null;
    Node left = null;
    Node right = null;
    int key = 0;

    public Node(Dancer d) {
        String key = d.getHeight() + (d.getGender().name().equals("MALE") ? "0" : "1") + d.getID();
        this.dancer = d;
        this.key = Integer.parseInt(key);
    }

    public boolean hasRight() {
        if (this.right != null) return true;
        else return false;
    }

    public boolean hasLeft() {
        if (this.left != null) return true;
        else return false;
    }

    public boolean hasParent() {
        if (this.parent != null) return true;
        else return false;
    }
    public boolean isMale() {
        if (this.dancer.getGender().name().equals("MALE"))
            return true;
        else return false;
    }
}