package ee.ttu.algoritmid.dancers;

/**
 * Created by rossm on 10/28/2017.
 */
import java.util.Random;
import org.apache.tools.ant.types.Assertions;

import java.util.AbstractMap.SimpleEntry;

import static ee.ttu.algoritmid.dancers.Dancer.Gender.FEMALE;
import static ee.ttu.algoritmid.dancers.Dancer.Gender.MALE;

public class Test {

    public static HW01 listOfDancers = new HW01();
	public static void main(String[] args) {

/*		for(int i=1;i<100000;i++) {
			Random rand = new Random();
			int  randHeight = rand.nextInt(200) + 1;
			Dancer.Gender randSex = rand.nextBoolean() ? FEMALE : MALE;

			Dancer dancer1 = new Dancer() {
				@Override public Gender getGender() {return randSex;}
				@Override public int getID() {return randHeight;}
				@Override public int getHeight() {return randHeight;}
			};
			listOfDancers.findPartnerFor(dancer1);
		}*/

		Dancer dancer1 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 85;}
			@Override public int getHeight() {return 85;}
		};
		Dancer dancer2 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 85;}
			@Override public int getHeight() {return 85;}
		};
		Dancer dancer3 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 40;}
			@Override public int getHeight() {return 40;}
		};
		Dancer dancer4 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 90;}
			@Override public int getHeight() {return 90;}
		};
		/*Dancer dancer5 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 90;}
			@Override public int getHeight() {return 90;}
		};
		Dancer dancer6 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 40;}
			@Override public int getHeight() {return 40;}
		};
		/////////////////////////////////////////////////////////////
		Dancer dancer7 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 60;}
			@Override public int getHeight() {return 60;}
		};
		Dancer dancer8 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 100;}
			@Override public int getHeight() {return 100;}
		};
		Dancer dancer9 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 90;}
			@Override public int getHeight() {return 90;}
		};
		Dancer dancer10 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 60;}
			@Override public int getHeight() {return 60;}
		};
		Dancer dancer11 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 100;}
			@Override public int getHeight() {return 100;}
		};
		Dancer dancer12 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 90;}
			@Override public int getHeight() {return 90;}
		};*/
		/////////////////////////////////////////////////////////////
		/*Dancer dancer13 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 10;}
			@Override public int getHeight() {return 10;}
		};
		Dancer dancer14 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 81;}
			@Override public int getHeight() {return 81;}
		};
		Dancer dancer15 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 51;}
			@Override public int getHeight() {return 51;}
		};
		Dancer dancer16 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 111;}
			@Override public int getHeight() {return 111;}
		};
		Dancer dancer17 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 49;}
			@Override public int getHeight() {return 49;}
		};
		Dancer dancer18 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 109;}
			@Override public int getHeight() {return 109;}
		};
		Dancer dancer19 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 51;}
			@Override public int getHeight() {return 51;}
		};
		Dancer dancer20 = new Dancer() {
			@Override public Gender getGender() {return FEMALE;}
			@Override public int getID() {return 111;}
			@Override public int getHeight() {return 111;}
		};
		Dancer dancer21 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 141;}
			@Override public int getHeight() {return 141;}
		};
		Dancer dancer22 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 121;}
			@Override public int getHeight() {return 121;}
		};
		Dancer dancer23 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 131;}
			@Override public int getHeight() {return 131;}
		};
		Dancer dancer24 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 101;}
			@Override public int getHeight() {return 101;}
		};
		Dancer dancer25 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 112;}
			@Override public int getHeight() {return 112;}
		};
		Dancer dancer26 = new Dancer() {
			@Override public Gender getGender() {return MALE;}
			@Override public int getID() {return 111;}
			@Override public int getHeight() {return 111;}
		};*/
		listOfDancers.returnWaitingList();
		//listOfDancers.findPartnerFor(null);
		listOfDancers.findPartnerFor(dancer1);
		listOfDancers.findPartnerFor(dancer2);
		listOfDancers.findPartnerFor(dancer3);
		listOfDancers.findPartnerFor(dancer4);
/*		listOfDancers.findPartnerFor(dancer5);
		listOfDancers.findPartnerFor(dancer6);
		listOfDancers.findPartnerFor(dancer7);
		listOfDancers.findPartnerFor(dancer8);
		listOfDancers.findPartnerFor(dancer9);
		listOfDancers.findPartnerFor(dancer10);
		listOfDancers.findPartnerFor(dancer11);
		listOfDancers.findPartnerFor(dancer12);*/
		/*listOfDancers.findPartnerFor(dancer13);
		listOfDancers.findPartnerFor(dancer14);
		listOfDancers.findPartnerFor(dancer15);
		listOfDancers.findPartnerFor(dancer16);
		listOfDancers.findPartnerFor(dancer17);
		listOfDancers.findPartnerFor(dancer18);
		listOfDancers.findPartnerFor(dancer19);
		listOfDancers.findPartnerFor(dancer20);
		listOfDancers.findPartnerFor(dancer21);
		listOfDancers.findPartnerFor(dancer22);
		listOfDancers.findPartnerFor(dancer23);
		listOfDancers.findPartnerFor(dancer24);
		listOfDancers.findPartnerFor(dancer25);
		listOfDancers.findPartnerFor(dancer26);*/
/*		listOfDancers.returnWaitingList();*/
		listOfDancers.returnWaitingList();
		listOfDancers.returnWaitingList();

		System.out.println("Left in queue:");
		for (Dancer dancer : listOfDancers.returnWaitingList()) {
			System.out.println("Height: " + dancer.getHeight() + ", " + (dancer.getGender().name().equals("MALE") ? "Male": "Female"));
		}


    }/**/
}
