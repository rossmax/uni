package ee.ttu.algoritmid.tsp;

import java.util.ArrayList;
import java.util.List;

public class Node implements Comparable<Node> {
    public int bound;
    public int level;

    public List<Integer> path = new ArrayList<>();

    @Override
    public int compareTo(Node node) {
        if (this.bound < node.bound)
            return -1;
        if (this.bound > node.bound)
            return 1;
        return 0;
    }
}
