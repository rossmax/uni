package ee.ttu.algoritmid.tsp;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

public class TSP {

  /*  File symmetric = new File("\\symmetric.in");
    File asymmetric = new File("\\asymmetric.in");
    File eesti = new File("\\eesti.in");*/

    public static int bestResult;
    public static int counter = 0;



    /* Depth first search */
    public static List<Integer> depthFirst(int[][] adjacencyMatrix) {

        boolean symetric = true;
        counter = 0;

        bestResult = Integer.MAX_VALUE;
        if (adjacencyMatrix.length == 0) {
            return Arrays.asList();
        }
        if (adjacencyMatrix.length == 1) {
            return Arrays.asList(0);
        }

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (adjacencyMatrix[i][j] != adjacencyMatrix[j][i]) {
                    symetric = false;
                }
            }
        }

        int[] pasCities = new int[adjacencyMatrix.length + 1];
        int[] bestPath = new int[adjacencyMatrix.length + 1];

        search(adjacencyMatrix, 0, 0, 0, bestPath, pasCities);

        List<Integer> result = new ArrayList<Integer>();

        if (!symetric) {
            for (int index = 0; index < bestPath.length; index++) {
                result.add(bestPath[index]);
            }
        } else {
            for (int index = bestPath.length - 1; index >= 0; index--)
            {
                result.add(bestPath[index]);
            }
        }


        return result;
    }


    /**
     *  DFS symmetric time: 54 milliseconds 11x11 (with pruning)
     *  DFS symmetric time: 804 milliseconds 11x11 (without pruning)
     *
     * Depth-first search (with pruning)
     */
    public static void search(int[][] matrix, int currentCity, int passedDistance, int passed, int[] bestPath, int[] passedCities) {
        counter++;
        // Create new vertex
        passedCities[passed] = currentCity;

        if (passed == matrix.length - 1) {
            passedDistance += matrix[currentCity][0];
            if (passedDistance < bestResult) {
                bestResult = passedDistance;
                System.arraycopy(passedCities, 0, bestPath, 0, passedCities.length);
            }
        } else {
            for (int i = 0; i < matrix.length; ++i) {
                loops:
                if (passedDistance + matrix[currentCity][i] < bestResult && i != currentCity) {
                    // Check that vertex is not used in the tree
                    for (int j = 0; j <= passed; j++)
                        if (passedCities[j] == i)
                            break loops;
                    search(matrix, i, passedDistance + matrix[currentCity][i], passed + 1, bestPath, passedCities);
                }
            }
        }
      }

    /**
     *  Depth-first search (without pruning)
     *
     *
     *  DFS symmetric time: 54 milliseconds 11x11 (with pruning)
     *  DFS symmetric time: 804 milliseconds 11x11 (without pruning)
     *
     *
     *  Conclusion: DFS with pruning is faster than without pruning
     */
    public static void search2(int[][] matrix, int currentCity, int passedDistance, int passed, int[] bestPath, int[] passedCities) {
        counter++;
        // Create new vertex
        passedCities[passed] = currentCity;

        if (passed == matrix.length - 1) {
            passedDistance += matrix[currentCity][0];
            if (passedDistance < bestResult) {
                bestResult = passedDistance;
                System.arraycopy(passedCities, 0, bestPath, 0, passedCities.length);
            }
        } else {
            for (int i = 0; i < matrix.length; ++i) {
                loops:
                if (i != currentCity) {
                    // Check that vertex is not used in the tree
                    for (int j = 0; j <= passed; j++)
                        if (passedCities[j] == i)
                            break loops;
                    search2(matrix, i, passedDistance + matrix[currentCity][i], passed + 1, bestPath, passedCities);
                }
            }
        }
    }



    /* Best first search */
    public static List<Integer> bestFirst(int[][] adjacencyMatrix) {

        if (adjacencyMatrix.length == 0) {

            return Arrays.asList();
        }
        if (adjacencyMatrix.length == 1) {

            return Arrays.asList(0);
        }

        PriorityQueue<Node> pq = new PriorityQueue<>();
        List<Integer> bestPath = new ArrayList<>();

        Node elem = new Node();

        elem.level = 0;
        elem.bound = bound(adjacencyMatrix, elem);
        elem.path.add(0);

        bestResult = Integer.MAX_VALUE;
        pq.add(elem);

        while(!pq.isEmpty()) {
            Node el = pq.remove();

            if (el.bound < bestResult) {
                Node u = new Node();
                u.level = el.level + 1;
                for (int i = 1; i < adjacencyMatrix.length; ++i) {
                    if (el.path.contains(i))
                        continue;

                    u.path.clear();

                    for (int k : el.path)
                        u.path.add(k);
                    u.path.add(i);
                    if (u.level == adjacencyMatrix.length - 1) {
                        u.path.add(0);

                        if (length(adjacencyMatrix, u) < bestResult) {
                            bestResult = length(adjacencyMatrix, u);
                            bestPath = u.path;
                        }
                    } else {
                        u.bound = bound(adjacencyMatrix, u);

                        Node n = new Node();
                        n.bound = u.bound;
                        n.level = u.level;

                        for (int k : u.path)
                            n.path.add(k);

                        if (u.bound < bestResult)
                            pq.add(n);
                    }
                }
            }
        }



        return bestPath;
    }

    public static int bound(int[][] matrix, Node node) {
        int bound = 0;

         for (int i = 0; i < matrix.length; ++i) {
            loops: {
                for (int j = 0; j < node.path.size() - 1; ++j)
                    if (node.path.get(j) == i)
                        break loops;

                int min = Integer.MAX_VALUE;
                for (int j = 0; j < matrix.length; ++j) {
                    loops2: {
                        for (int k = 1; k < node.path.size(); ++k)
                            if (node.path.get(k) == j)
                                break loops2;

                        if (j == 0 && node.path.size() > 0 && node.path.get(node.path.size() - 1) == i)
                            continue;

                        if (min > matrix[i][j] && matrix[i][j] != 0)
                            min = matrix[i][j];
                    }
                }
                bound += min;
            }
        }

        for (int i = 0; i < node.path.size() - 1; ++i)
            bound += matrix[node.path.get(i)][node.path.get(i + 1)];

        return bound;
    }

    public static int length(int[][] matrix, Node node) {
        int l = 0;
        for (int i = 0; i < node.path.size() - 1; ++i)
            l += matrix[node.path.get(i)][node.path.get(i + 1)];
        return l;
    }

    /* Nodes viewed in last matrix to find the solution (should be zeroed at the beginnig of search) */
    public static BigInteger getCheckedNodesCount() {

        return BigInteger.valueOf(counter);
    }

    public static void main(String[] args) throws Exception {
        final int[][] msym1 = MatrixLoader.loadFile("symmetric.in", 11);
        final int[][] msym2 = MatrixLoader.loadFile("asymmetric.in", 11);
        final int[][] msym3 = MatrixLoader.loadFile("eesti.in", 11);

        long time1 = System.currentTimeMillis();
        System.out.println(depthFirst(msym1));
        long time2 = System.currentTimeMillis();
        System.out.println("DFS symmetric time: " + (time2 - time1) + " milliseconds");
        System.out.println("///////////////////////////////////////////////////////");

        long time3 = System.currentTimeMillis();
        System.out.println(depthFirst(msym2));
        long time4 = System.currentTimeMillis();
        System.out.println("DFS asymmetric time: " + (time4 - time3) + " milliseconds");
        System.out.println("///////////////////////////////////////////////////////");

        long time5 = System.currentTimeMillis();
        System.out.println(depthFirst(msym3));
        long time6 = System.currentTimeMillis();
        System.out.println("DFS eesti time: " + (time6 - time5) + " milliseconds");
        System.out.println("///////////////////////////////////////////////////////");

        long time7 = System.currentTimeMillis();
        System.out.println(bestFirst(msym1));
        long time8 = System.currentTimeMillis();
        System.out.println("BFS symmetric time: " + (time8 - time7) + " milliseconds");
        System.out.println("///////////////////////////////////////////////////////");

        long time9 = System.currentTimeMillis();
        System.out.println(bestFirst(msym2));
        long time10 = System.currentTimeMillis();
        System.out.println("BFS asymmetric time: " + (time10 - time9) + " milliseconds");
        System.out.println("///////////////////////////////////////////////////////");

        long time11 = System.currentTimeMillis();
        System.out.println(bestFirst(msym3));
        long time12 = System.currentTimeMillis();
        System.out.println("BFS eesti time: " + (time12 - time11) + " milliseconds");
        System.out.println("///////////////////////////////////////////////////////");
    }
}