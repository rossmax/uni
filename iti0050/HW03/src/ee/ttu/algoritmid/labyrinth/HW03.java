package ee.ttu.algoritmid.labyrinth;

import java.util.*;
import java.io.IOException;
import java.net.URISyntaxException;

public class HW03 {
    private MazeRunner mazeRunner;
    private String right;
    private String left;
    private String back;
    private String direct;

    public HW03(String fileName) throws IOException, URISyntaxException {
        mazeRunner = new MazeRunner(fileName);

    }

    public MazeRunner getMazeRunner() {
        return mazeRunner;
    }

    /**
     * Returns the list of steps to take to get from beginning ("B") to
     * the treasure ("T").
     * @return  return the steps taken as a list of strings (e.g., ["E", "E", "E"])
     *          return null if there is no path (there is no way to get to the treasure).
     */
    public List<String> solve() {
        List<String> result = new ArrayList<>();
        getDirect("N");

        String move;

        while (mazeRunner.scan().get(1).get(1) != -2) {
            move = mazeRunner.move(left) ? left : (mazeRunner.move(direct) ? direct : (mazeRunner.move(right)
                    ? right : (mazeRunner.move(back) ? back : null)));
            if (!move.equals(direct) && move != null) {
                result.add(move);
                getDirect(move);
            } else result.add(move);
            if (move == null) return null;
            if (mazeRunner.scan().get(1).get(1) == 0) return null;
        }
        return result;
    }

    public void getDirect(String direct) {
        this.direct = direct;
        back = (direct.equals("S") ? "N" : (direct.equals("N") ? "S" : (direct.equals("E") ? "W" : "E")));
        left = (direct.equals("S") ? "E" : (direct.equals("N") ? "W" : (direct.equals("E") ? "N" : "S")));
        right = (direct.equals("S") ? "W" : (direct.equals("N") ? "E" : (direct.equals("E") ? "S" : "N")));
    }

    /*public static void main(String[ ] args) throws IOException, URISyntaxException {
        HW03 game;
        game = new HW03("ns100b.maze");
        System.out.println(game.solve());
    }*/
}
