

import ee.ttu.algoritmid.labyrinth.HW03;
import ee.ttu.algoritmid.labyrinth.MazeRunner;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;


public class Main {
    /*
   Main function for testing purposes.
   */
    public static void main(String[] args) throws IOException, URISyntaxException {
        String filename = "ns100b.maze";
        HW03 solver = new HW03(filename);
        List<String> directions = solver.solve();
        for (String i : directions) {
            System.out.println(i);
        }
        System.out.println(getDistance(directions, filename));
    }

    /*
    If treasure is found, then returns distance to get there.
    If treasure is not found, then returns -1.
    */
    public static int getDistance(List<String> directions, String mazeFilename) {
        MazeRunner runner;
        try {
            runner = new MazeRunner(mazeFilename);
        } catch(Exception e) {
            return -1;
        }
        int distance = 0;
        boolean treasureFound = false;
        for (String direction : directions) {
            int moveDistance = singleMoveDistance(direction, runner);
            if (moveDistance == -2) {
                treasureFound = true;
            }
            else {
                distance += moveDistance;
            }
        }
        return distance;
    }

    static int singleMoveDistance(String direction, MazeRunner runner) {
        List<List<Integer>> scanned = runner.scan();
        int distance = 0;
        if (direction == "N") {
            distance += scanned.get(0).get(1);
        }
        else if (direction == "S") {
            distance += scanned.get(2).get(1);
        }
        else if (direction == "W") {
            distance += scanned.get(1).get(0);
        }
        else if (direction == "E") {
            distance += scanned.get(1).get(2);
        }
        else {
            throw new IllegalArgumentException("Wrong direction.");
        }

        // Wall
        if (distance == -1) {
            throw new IllegalArgumentException("Ran into a wall.");
        }

        //move there
        runner.move(direction);
        return distance;

    }



}
