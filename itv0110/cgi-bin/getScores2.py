#!/usr/bin/python

import cgitb; cgitb.enable()
import cgi
import json
from datetime import datetime

filename = '../cgi-bin/tmp/scores.txt'
file = open(filename, 'r')

form = cgi.FieldStorage()

filter = '';	
if 'filter' in form:
	filter = form['filter'].value


data = []
for line in file:
	line = line.strip()
	result = line.split('\t')

	if filter != '':
		if filter == result[0]:
			data.append({
				'nick': result[0],
				'size': result[1],
				'mines': result[2],
				'clicks': result[3],
				'res': result[4],
                		'time': result[5],
				'currenttime': result[6],
				'currenttimetime': result[7],
			})
		elif filter == result[6]:
			data.append({
				'nick': result[0],
				'size': result[1],
				'mines': result[2],
				'clicks': result[3],
				'res': result[4],
                		'time': result[5],
				'currenttime': result[6],
				'currenttimetime': result[7],
			})
	else:
		data.append({
				'nick': result[0],
				'size': result[1],
				'mines': result[2],
				'clicks': result[3],
				'res': result[4],
                		'time': result[5],
				'currenttime': result[6],
				'currenttimetime': result[7],
		})
file.close()

print 'Content-type: application/json\n'
print json.dumps(data)
