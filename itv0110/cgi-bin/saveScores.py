#!/usr/bin/python


# Import modules for CGI handling
import cgitb; cgitb.enable()
import cgi
from datetime import datetime
import json
import random

# Create instance of FieldStorage
form = cgi.FieldStorage()

nick = form['nick'].value
size = form['size'].value
mines = form['mines'].value
clicks = form['clicks'].value
res = form['res'].value
time = form['time'].value
currenttime = datetime.now().strftime('%H:%M:%S')
currenttimetime = datetime.now().strftime('%d-%m-%Y')


file = open('../cgi-bin/tmp/scores.txt', 'a')
file.write(nick  + '\t')
file.write(size  + '\t')
file.write(mines  + '\t')
file.write(clicks  + '\t')
file.write(res  + '\t')
file.write(time  + '\t')
file.write(currenttime  + '\t')
file.write(currenttimetime  + '\n')

file.close()

print 'Content-type: application/json\n'
