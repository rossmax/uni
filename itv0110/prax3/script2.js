        var gridx;
        var gridy;
        var mines;
        var board;
        var clicks = 0;
    
        var nick;    
        var startTime;                 
        var timer = false;
        var gamestatus = 0;
        var startTime=0, stopTime=0;
        

        
        function imagePreload() {
            var i, ids = [1, 2, 3, 4, 5, 6, 7];
            window.status = "Preloading images...please wait";
            for (i = 0; i < ids.length; ++i) {
                var img = new Image,
                    name = "pic" + ids[i] + ".gif";
                img.src = name;
                preloaded[i] = img;
            }
            window.status = "";
        }
                
        function myFunction() {
            
            if(checkValues()){
                
                
                gridx = document.getElementById("Size").value;
                gridy = document.getElementById("Size").value;
                mines = document.getElementById("Mines").value;
                nick = document.getElementById("Nick").value;
                startTime = new Date().getTime(); 
                
                

                console.log(gridx);
                console.log(gridy);
                console.log(mines);
                console.log(nick)
                
                
                
                board = makeBoard(gridx,mines);
                document.getElementById('smile').setAttribute("src", "smile.png");
                
                var fullBoard = createFullBoard(board);
                console.log(board);
                var showBoard = showGrid(board);

                
            }
            return "";
                        
        }
        

        

        
        function showGrid(board) {
            var y, x;
            for (y = 0; y < gridy; ++y) {
                for (x = 0; x < gridx; ++x) { 
                        document.getElementById('grid').innerHTML += '<a href="javascript:gridClick(' + y + ',' + x + ');"><img id="pic' + y + '_' + x + '" src="pic1.gif" width=40 height=40></a>';
                }
                document.getElementById('grid').innerHTML += '<br>';
            }
        }
        
        
        function gridClick(y, x) {
            clicks++;

            
            if(gamestatus == 1 || gamestatus == -1) {
                return;
            }
            
    
            
            var name = "pic" + y + "_" + x;
            

            if (board[y][x] == -1) {              
                showMeAll();
                endGame(-1);
                
            }
            if (board[y][x] == 0) {
                document.getElementById(name).setAttribute("src", "pic7.gif");
                showMeNull(x,y);
                var x = getCount();
                if(x == mines) endGame(1);
                
            }
            if (board[y][x] == 1) {
                
                document.getElementById(name).setAttribute("src", "pic2.gif");
                var x = getCount();
                if(x == mines) endGame(1);
                
        }
            if (board[y][x] == 2) {

                document.getElementById(name).setAttribute("src", "pic3.gif");
                var x = getCount();
                if(x == mines) endGame(1);
                

            }
            if (board[y][x] == 3) {

                document.getElementById(name).setAttribute("src", "pic4.gif");
                var x = getCount();
                if(x == mines)  endGame(1);
                

            }
            if (board[y][x] == 4) {
  
                document.getElementById(name).setAttribute("src", "pic5.gif");
                var x = getCount();
                if(x == mines)  endGame(1);
            }
        }
        
        function reloadBroad() {
            location.reload();
            
        }
        
        function getCount(){
                var count = 0;
                var allElements = document.getElementsByTagName('*');
                for (var x = 0; x < allElements.length; x++) {
                    if (allElements[x].getAttribute("src") == "pic1.gif") count++;
                }

            return count;

        }
        
        function showMeAll() {    
                for (y = 0; y < board.length; ++y) {
                    for (x = 0; x < board.length; ++x) { 
                            var name = "pic" + y + "_" + x;
                            if (board[y][x] == -1) {
                                document.getElementById(name).setAttribute("src", "pic6.gif");
                            }

                    }
                }
        }
        
        
        function showMeNull(y,x) {
            
            
            var neighboursArray = neighbours(board.length,x,y)


            for (var u = 0; u < neighboursArray.length; u++) {

                    var changeNeiboards = neighboursArray[u];
                    var name = "pic" + changeNeiboards[0] + "_" + changeNeiboards[1];

                    if (board[changeNeiboards[0]][changeNeiboards[1]] == 0) {
                         document.getElementById(name).setAttribute("src", "pic7.gif");
                        

                         }
                    if (board[changeNeiboards[0]][changeNeiboards[1]] == 1) {
                                    document.getElementById(name).setAttribute("src", "pic2.gif");
                                }
                    if (board[changeNeiboards[0]][changeNeiboards[1]] == 2) {

                                    document.getElementById(name).setAttribute("src", "pic3.gif");
                                }
                    if (board[changeNeiboards[0]][changeNeiboards[1]] == 3) {

                                    document.getElementById(name).setAttribute("src", "pic4.gif");
                    }
                   if (board[changeNeiboards[0]][changeNeiboards[1]] == 4) {

                                    document.getElementById(name).setAttribute("src", "pic5.gif");
                                }
                            
                        }
            }


        
        
        function checkValues() {
            
            if(document.getElementById("Size").value <= 5) {
                alert("Size is to small! Value must be more than 5");
                return false;
            }
            if(document.getElementById("Mines").value <= 1) {
                alert("Mines value is to small! Value must be more than 1");
                return false;
            } 
            
            var element =  document.getElementById("pic0_0");
            if (typeof(element) != 'undefined' && element != null) {
              console.log("Reset grid");
                gamestatus = 0;
                var myNode = document.getElementById("grid");
                var fc = myNode.firstChild;
                while( fc ) {
                    myNode.removeChild( fc );
                    fc = myNode.firstChild;
                }
            }
            return true;
            
        }
        
        function createFullBoard(board) {
            for (var x = 0; x < board.length; x++) {
                for (var y = 0; y < board.length; y++) {
                    
                    
                    if (board[x][y] == -1) {
                        var neighboursArray = neighbours(board.length,x,y)
                        
                        
                        for (var i = 0,m = 0; i < neighboursArray.length; i++,m++) {
                            
                            var changeNeiboards = neighboursArray[i];
                            
                            if (board[changeNeiboards[0]][changeNeiboards[1]] != -1 ) board[changeNeiboards[0]][changeNeiboards[1]] += 1;
                            
                        }
                    }
                }
            }
            
        }
        
        function endGame(result) {

        if(result == -1) {
            document.getElementById('smile').setAttribute("src", "sad.png");
            gamestatus = -1;
            saveScores();
        }
        if(result == 1) {
            document.getElementById('smile').setAttribute("src", "win.png");
            gamestatus = 1;
            saveScores();
        }
        /////
        }
        
        
        
        
        function makeBoard(size,mines) {
          var board=[]; 

          if (mines >= gridx * gridy) throw "too many bombs for this size";

          // initialize board, filling with zeros
          for (var x = 0; x < gridx; x++) {
            board[x]=[]; // insert empty subarray
            for (var y = 0; y < gridx; y++) board[x][y]=0;
          }

          // now fill board with bombs in random positions
          var i = mines;
            
          while (i > 0) {
            // generate random x and y in range 0...size-1
            x = Math.floor(Math.random() * gridx);
            y = Math.floor(Math.random() * gridy);
            // put bomb on x,y unless there is a bomb already
            if (board[x][y] != -1) {
              board[x][y] = -1;
              i--; // bomb successfully positioned, one less to go
              //console.log("positioned "+x+", "+y+" yet to go "+i);
            }
          }
        
          return board;
        }
        
        function neighbours(size,x,y) {
          var list=[];
          for (var i=-1; i<=1; i++) {    
            for (var j=-1; j<=1; j++) {
              // square is not a neighbour of itself
              if (i==0 && j==0) continue;
              // check whether the the neighbour is inside board bounds
              if ((x+i)>=0 && (x+i)<size && (y+j)>=0 && (y+j)<size) {
                list.push([x+i,y+j]);  
              }
            }
          }
          return list;
        }


        function saveScores(){
            stopTime = new Date().getTime();
            var time = (stopTime - startTime) / 1000;
	    var res;
	    if (gamestatus == -1){res = "lose";}
	    else res = "win";

            var data = {
            'nick':nick,
            'size':gridx,
            'mines':mines,
            'clicks':clicks,
            'res':res,
            'time':time,
            }

            $.get("../cgi-bin/saveScores.py", data);
            console.log(data);
        }
