<?php

	// this will avoid mysql_connect() deprecation error.
	error_reporting( ~E_DEPRECATED & ~E_NOTICE );
	// but I strongly suggest you to use PDO or MySQLi.
	
	define('DBHOST', 'localhost');
	define('DBUSER', 'st2014');
	define('DBPASS', 'progress');
	define('DBNAME', 'st2014');
	
	$conn = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME);
	
	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}