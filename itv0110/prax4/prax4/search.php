<?php
	ob_start();
	session_start();
	require_once 'dbconnect.php';
	
if ( isset($_POST['find_submit']) ) {

$fiend_input = trim($_POST['fiend_input']);
$fiend_input = strip_tags($fiend_input);
$fiend_input = htmlspecialchars($fiend_input);

if (empty($fiend_input)) {
      $error = true;
      $fiend_inputError = "Invalid.";
    } else if (strlen($fiend_input) < 3) {
      $error = true;
      $fiend_inputError = "Input must have atleat 3 characters.";
    } else if (!preg_match("/^[a-zA-Z ]+$/",$fiend_input)) {
      $error = true;
      $fiend_inputError = "Invalid.";
    }

$serch=mysqli_query($conn, "SELECT userId, userName FROM max3p_users WHERE userName LIKE '%" .$fiend_input. "%'");
       $search_results=array();
       while($userRow=mysqli_fetch_array($serch)) {
          $search_results[] = $userRow;
        }
               
        } else {
          $search_results=array();
        }

	if( !isset($_SESSION['user']) ) {
		header("Location: index.php");
		exit;
	}

  $res2=mysqli_query($conn, "SELECT userId, userName, userEmail, userDescr FROM max3p_users WHERE userId='".$_SESSION['user']."'");
  $row=mysqli_fetch_array($res2);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome - <?php echo $row['userEmail']; ?></title>
<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"  />
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://dijkstra.cs.ttu.ee/~Maksim.Kalistratov/prax4/home.php">Home</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="http://dijkstra.cs.ttu.ee/~Maksim.Kalistratov/prax4/profile.php">Profile</a></li>
            <form action="/~Maksim.Kalistratov/prax4/search.php" method="POST" class="navbar-form navbar-left">
                    <div class="form-group">
                        <input type="text" name="fiend_input" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" name="find_submit" class="btn btn-default">Find</button>
                </form>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			  <span class="glyphicon glyphicon-user"></span>&nbsp;Hi' <?php echo $row['userEmail']; ?>&nbsp;<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sign Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav> 

	<div id="wrapper">

	<div class="container">

    <div class="row">

        
    	<div class="page-header">

    	</div>
        <div class="col-lg-12">
        <h3>Search Results:</h3>
        <?php foreach($search_results as $result) { ?>
        <div class="col-md-4">
            <div class="panel panel-info">
                <div id="head" class="panel-heading"><h3 class="panel-title"><a href="profile.php?user=<?php echo $result['userId']; ?>"><?php echo $result['userName']; ?></a></h3></div>
            </div>
        </div>
         
         <?php } ?>

        </div>
        </div>
        
        
    
    </div>
    
    </div>
    
    <script src="assets/jquery-1.11.3-jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
</body>
</html>
<?php ob_end_flush(); ?>